import torch
from diff_rasterization import _C

def rasterize_layered_points(
    points,
    features,
    alphas,
    inv_cov,
    max_radius,
    image_height,
    image_width,
    zfar,
    znear,
    gamma,
    fastVersion=False
):

    return _RasterizeLayeredPoints.apply(
        points,
        features,
        alphas,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma,
        fastVersion
    )

class _RasterizeLayeredPoints(torch.autograd.Function):
    @staticmethod
    def forward(
        ctx,
        points,
        features,
        alphas,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma,
        fastVersion
    ):

        args = (
            points,
            features,
            alphas,
            inv_cov,
            max_radius,
            image_height,
            image_width,
            zfar,
            znear,
            gamma
        )
        idxs, k_idxs, color, total_w, accumulated_color, alpha_map = _C.rasterize_layered_points(*args)

        ctx.znear = znear
        ctx.zfar = zfar
        ctx.gamma = gamma
        ctx.max_radius = max_radius
        ctx.fastVersion = fastVersion
        ctx.save_for_backward(points, features, alphas, inv_cov, idxs, k_idxs, total_w, accumulated_color, alpha_map)

        return color, alpha_map

    @staticmethod
    def backward(ctx, grad_out_color, grad_out_alpha_map):
        grad_points = None
        grad_colors = None
        grad_alphas = None
        grad_inv_cov = None
        grad_max_radius = None
        grad_image_height = None
        grad_image_width = None
        grad_zfar = None
        grad_znear = None
        grad_gamma = None
        grad_fastVersion = None
        znear = ctx.znear
        zfar = ctx.zfar
        gamma = ctx.gamma
        max_radius = ctx.max_radius
        fastVersion = ctx.fastVersion
        points, features, alphas, inv_cov, idxs, k_idxs, total_w, accumulated_color, alpha_map = ctx.saved_tensors
        args = (points, features, alphas, inv_cov, max_radius,
                idxs, k_idxs, accumulated_color, total_w, alpha_map,
                znear, zfar, gamma, fastVersion, grad_out_color, grad_out_alpha_map)
        grad_points, grad_colors, grad_inv_cov, grad_alphas = _C.rasterize_layered_points_backward(*args)
        grads = (
            grad_points,
            grad_colors,
            grad_alphas,
            grad_inv_cov,
            grad_max_radius,
            grad_image_height,
            grad_image_width,
            grad_zfar,
            grad_znear,
            grad_gamma,
            grad_fastVersion
        )
        return grads


def rasterize_points_cache(
    points,
    features,
    alphas,
    inv_cov,
    max_radius,
    image_height,
    image_width,
    zfar,
    znear,
    gamma
):

    args = (
        points,
        features,
        alphas,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma
    )

    return _C.rasterize_points_cache(*args)

def rasterize_points(
    points,
    features,
    alphas,
    inv_cov,
    max_radius,
    image_height,
    image_width,
    zfar,
    znear,
    gamma,
    fast_version=False
):

    return _RasterizePoints.apply(
        points,
        features,
        alphas,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma,
        fast_version
    )


class _RasterizePoints(torch.autograd.Function):
    @staticmethod
    def forward(
        ctx,
        points,  # (P, 3)
        colors,  # (P, C)
        alphas,
        inv_cov,  # (P, 4)
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma,
        fast_version
    ):
        # TODO: Add better error handling for when there are more than
        # max_points_per_bin in any bin.
        args = (
            points,
            colors,
            alphas,
            inv_cov,
            max_radius,
            image_height,
            image_width,
            zfar,
            znear,
            gamma
        )
        idx, color, k_idxs, mask = _C.rasterize_points(*args)
        ctx.znear = znear
        ctx.zfar = zfar
        ctx.gamma = gamma
        ctx.fast_version = fast_version
        ctx.max_radius = max_radius
        ctx.save_for_backward(points, colors, alphas, inv_cov, idx, k_idxs)
        return idx, color, mask

    @staticmethod
    def backward(ctx, grad_idx, grad_out_color, grad_out_mask):
        grad_points = None
        grad_colors = None
        grad_alphas = None
        grad_inv_cov = None
        grad_max_radius = None
        grad_image_height = None
        grad_image_width = None
        grad_bin_size = None
        grad_zfar = None
        grad_znear = None
        grad_gamma = None
        znear = ctx.znear
        zfar = ctx.zfar
        gamma = ctx.gamma
        max_radius = ctx.max_radius
        fast_version = ctx.fast_version
        points, colors, alphas, inv_cov, idx, k_idxs = ctx.saved_tensors
        args = (points, colors, alphas, inv_cov, max_radius, idx, k_idxs, znear, zfar, gamma, grad_out_color, grad_out_mask, fast_version)
        grad_points, grad_colors, grad_inv_cov, grad_alphas = _C.rasterize_points_backward(*args)
        grads = (
            grad_points,
            grad_colors,
            grad_alphas,
            grad_inv_cov,
            grad_max_radius,
            grad_image_height,
            grad_image_width,
            grad_bin_size,
            grad_zfar,
            grad_znear,
            grad_gamma
        )
        return grads
