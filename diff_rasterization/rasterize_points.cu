#include <math.h>
#include <torch/extension.h>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <tuple>
#include <stdio.h>
#include "bitmask.cuh"
#include "rasterization_utils.cuh"

namespace {
    // A little structure for holding details about a pixel.
    struct Pix {
      float z; // Depth of the reference point.
      int32_t idx; // Index of the reference point.
      float dist2; // Euclidean distance square to the reference point.
      float g_w; // Alpha blending weight
    };

    __device__ inline bool operator<(const Pix& a, const Pix& b) {
      return a.z < b.z;
    }

    __device__ inline bool operator>(const Pix& a, const Pix& b) {
      return a.z > b.z;
    }

    __device__ inline bool operator<=(const Pix& a, const Pix& b) {
      return a.z <= b.z;
    }

    __device__ inline bool operator>=(const Pix& a, const Pix& b) {
      return a.z >= b.z;
    }

}

// ****************************************************************************
// *                          GK RASTERIZATION                             *
// ****************************************************************************


__global__ void BlendPointsGKCudaKernel(
    const float* points, // (P, 3)
    int32_t* point_idx, // (N, H, W, K)
    const float* colors, // (P, C)
    const float* alphas, // (P)
    const int32_t* k_idxs, // (N, H, W)
    const float* inv_cov, // (P, 4)
    const int max_radius,
    const float gamma,
    const int N,
    const int H,
    const int W,
    const int C,
    const int K,
    const float zfar,
    const float znear,
    float* color, // (N, 3, H, W)
    float* mask)
{
    const int radius2 = max_radius*max_radius;
    // One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;

    
    for (int i = tid; i < H * W; i += num_threads) {
        // Convert linear index to 3D index
        const int pix_idx = i % (H * W);

        const int yi = pix_idx / W;
        const int xi = pix_idx % W;

        Pix gathered_points[kMaxPointPerPixelLocal];
        int y_start = yi - max_radius;
        int y_finish = yi + max_radius;
        int x_start = xi - max_radius;
        int x_finish = xi + max_radius;

        int gathered_points_idx = 0;
        int gathered_points_idx_max = -1;
        float gathered_points_z_max = -1000000.0;
        for (int y_idx = y_start; y_idx < y_finish + 1; y_idx++) {
            for (int x_idx = x_start; x_idx <  x_finish + 1; x_idx++) {
                if (y_idx < 0 || y_idx > H - 1 || x_idx < 0 || x_idx > W - 1)
                    continue;
                int k = k_idxs[y_idx*W + x_idx];
                int idx = 0 * H * W * K + y_idx * W * K + x_idx * K + 0;
                for (int i=0; i<k && i<kMaxPointsPerPixel; i++) {
                    int p_idx = point_idx[idx + i];
                    float px_ndc = points[p_idx*3 + 0];
                    float py_ndc = points[p_idx*3 + 1];
                    float pz     = points[p_idx*3 + 2];
                    if (pz < 0)
                        // Don't render points behind the camera.
                        continue;

                    float alpha = alphas[p_idx];

                    float dx = NdcToPix(px_ndc, W) - xi;
                    float dy = NdcToPix(py_ndc, H) - yi;
                    float dist2 = dx*dx + dy*dy;
                    // Trim it to a circle
                    if (dist2 > radius2)
                        continue;

                    float inv_cov00 = inv_cov[p_idx*4 + 0];
                    float inv_cov01 = inv_cov[p_idx*4 + 1];
                    float inv_cov10 = inv_cov[p_idx*4 + 2];
                    float inv_cov11 = inv_cov[p_idx*4 + 3];

                    float power = (-1.0/2.0)*(inv_cov00*dx*dx + (inv_cov01+inv_cov10)*dx*dy + inv_cov11*dy*dy);
                    if (power>0.0) {
                        //printf("%f || %f %f %f %f?\n", power, inv_cov00, inv_cov01, inv_cov10, inv_cov11);
                        continue;
                    }
                    float g_w = alpha * exp(power);
                    if (g_w < 1/255.0)
                        continue;

                    // If more than kMaxPointPerPixelLocal we need to compare against the max z
                    // if we are closer we replace our selves and search for the max again
                    if (gathered_points_idx > kMaxPointPerPixelLocal - 1) {
                        if (pz < gathered_points_z_max) {
                            gathered_points[gathered_points_idx_max].idx = p_idx;
                            gathered_points[gathered_points_idx_max].dist2 = dist2;
                            gathered_points[gathered_points_idx_max].g_w = g_w;
                            gathered_points[gathered_points_idx_max].z = pz;

                            gathered_points_z_max = -1.0;
                            for (int j=0; j<gathered_points_idx; j++) {
                                if (gathered_points[j].z > gathered_points_z_max) {
                                    gathered_points_idx_max = j;
                                    gathered_points_z_max = gathered_points[j].z;
                                }
                            }
                        }
                    }
                    else {
                        if (pz > gathered_points_z_max) {
                            gathered_points_idx_max = gathered_points_idx;
                            gathered_points_z_max = pz;
                        }
                        gathered_points[gathered_points_idx].idx = p_idx;
                        gathered_points[gathered_points_idx_max].dist2 = dist2;
                        gathered_points[gathered_points_idx].g_w = g_w;
                        gathered_points[gathered_points_idx].z = pz;
                        gathered_points_idx++;
                    }
                }
            }
        }
        if (gathered_points_idx > 1)
        {
            QuickSortIterative(gathered_points, 0, gathered_points_idx - 1);
        }
        // BubbleSort(gathered_points, gathered_points_idx);

        float cum_alpha = 1.0;
        /* TODO: Adding iteratively to global memory can be slow, but dynamic allocation doesnt work. */
        /* TODO: Hard-Code number of features to see perfomance gain */
        //float result[3] = {0.0, 0.0, 0.0};
        //float* result = new float[C]();
        //float *result = (float *)malloc(3*sizeof(float));
        int k;
        for (k=0; k<gathered_points_idx; k++) {
            float g_w = gathered_points[k].g_w;
            float weight = cum_alpha * g_w;
            for (int ch=0; ch<C; ch++) {
                color[ch*H*W + yi*W + xi] += colors[gathered_points[k].idx*C + ch] * weight;
            }
            cum_alpha = cum_alpha * (1 - g_w);
            if (cum_alpha<0.001) {
                k = k+1;
                break;
            }
        }
        mask[yi*W + xi] = cum_alpha;
    }
}

__global__ void RasterizePointsGKCudaKernel(
    const float* points, // (P, 3)
    const int P,
    uint32_t* k_idxs, // (N, H, W)
    const int N,
    const int H,
    const int W,
    const int K,
    int32_t* point_idxs) // (N, H, W, K)
{
    // Simple version: One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    // TODO gkopanas more than 1 batches?
    for (int i = tid; i < P ; i += num_threads) {
        const float px_ndc = points[i * 3 + 0];
        const float py_ndc = points[i * 3 + 1];

        const float px = NdcToPix(px_ndc, W);
        const float py = NdcToPix(py_ndc, H);

        const int px_rounded = int(px + 0.5);
        const int py_rounded = int(py + 0.5);
        if (py_rounded < 0 || py_rounded > H - 1 || px_rounded < 0 || px_rounded > W - 1)
            continue;

        //int k_idx = atomicInc(&(k_idxs[0*H*W + py_rounded*W + px_rounded]), K + 1);
        int k_idx = atomicAdd(&(k_idxs[0*H*W + py_rounded*W + px_rounded]), 1);
        if (k_idx > K-1) {
            //printf("Pixel y:%d x:%d exceeded point projection limit %d\n", py_rounded, px_rounded, k_idx);
            continue;
            //assert(0);
        }

        int idx = 0 * H * W * K + py_rounded * W * K + px_rounded * K + k_idx;
        point_idxs[idx] = i;
    }
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePointsGKCuda(
    const torch::Tensor& points, // (P, 3)
    const torch::Tensor& colors, // (P, C)
    const torch::Tensor& alphas, // (P)
    const torch::Tensor& inv_cov, // (P, 4)
    const int max_radius,
    const int image_height,
    const int image_width,
    const float zfar,
    const float znear,
    const float gamma) {

  if (points.ndimension() != 2 || points.size(1) != 3) {
    AT_ERROR("points must have dimensions (num_points, 3)");
  }

  const int P = points.size(0);
  const int C = colors.size(1);
  const int N = 1; // batch size hard-coded
  const int H = image_height;
  const int W = image_width;

  auto int_opts = points.options().dtype(torch::kInt32);
  auto float_opts = points.options().dtype(torch::kFloat32);

  torch::Tensor point_idxs = torch::full({N, H, W, kMaxPointsPerPixel}, -1, int_opts);
  torch::Tensor k_idxs = torch::full({N, H, W}, 0, int_opts);
  torch::Tensor out_color = torch::full({N, C, H, W}, 0.0, float_opts);
  torch::Tensor mask = torch::full({N, 1, H, W}, 0, float_opts);

  const size_t blocks = 1024;
  const size_t threads = 64;

  RasterizePointsGKCudaKernel<<<blocks, threads>>>(
      points.contiguous().data<float>(),
      P,
      (unsigned int *)k_idxs.data<int32_t>(),
      N,
      H,
      W,
      kMaxPointsPerPixel,
      point_idxs.contiguous().data<int32_t>());

  cudaDeviceSynchronize();

  BlendPointsGKCudaKernel<<<blocks, threads>>>(
      points.contiguous().data<float>(),
      point_idxs.contiguous().data<int32_t>(),
      colors.contiguous().data<float>(),
      alphas.contiguous().data<float>(),
      k_idxs.data<int32_t>(),
      inv_cov.data<float>(),
      max_radius,
      gamma,
      N,
      H,
      W,
      C,
      kMaxPointsPerPixel,
      zfar,
      znear,
      out_color.contiguous().data<float>(),
      mask.contiguous().data<float>());

  return std::make_tuple(point_idxs, out_color, k_idxs, mask);
}


// ****************************************************************************
// *                            BACKWARD PASS                                 *
// ****************************************************************************
// TODO(T55115174) Add more documentation for backward kernel.
__global__ void RasterizePointsBackwardCudaKernel(
        const float *points, // (P, 3)
        const float *colors, // (P, C)
        const float *alphas, // (P)
        const float *inv_cov, // (P, 4)
        const int max_radius,
        const int32_t *idxs, // (N, H, W, K)
        const int32_t *k_idxs,
        const int N,
        const int P,
        const int C,
        const int H,
        const int W,
        const int K,
        const float znear,
        const float zfar,
        const float gamma,
        float* grad_out_color,
        float* grad_out_alpha_map,
        float* grad_points,
        float* grad_colors,
        float* grad_inv_cov,
        float* grad_alpha,
        bool fast_version) 
    {

    const int radius2 = max_radius*max_radius;
    // One thread per output pixel
    const int num_threads = gridDim.x * blockDim.x;
    const int tid = blockDim.x * blockIdx.x + threadIdx.x;
    for (int i = tid; i < H * W; i += num_threads) {
        // Convert linear index to 3D index
        const int pix_idx = i % (H * W);

        const int yi = pix_idx / W;
        const int xi = pix_idx % W;

        Pix gathered_points[kMaxPointPerPixelLocal];
        const int y_start = yi - max_radius;
        const int y_finish = yi + max_radius;
        const int x_start = xi - max_radius;
        const int x_finish = xi + max_radius;

        int gathered_points_idx = 0;
        int gathered_points_idx_max = -1;
        float gathered_points_z_max = -1000000.0;
        for (int y_idx = y_start; y_idx < y_finish + 1; y_idx++) {
            for (int x_idx = x_start; x_idx <  x_finish + 1; x_idx++) {
                if (y_idx < 0 || y_idx > H - 1 || x_idx < 0 || x_idx > W - 1)
                    continue;
                const int k = k_idxs[y_idx*W + x_idx];
                int idx = 0 * H * W * K + y_idx * W * K + x_idx * K + 0;
                for (int i=0; i<k && i<kMaxPointsPerPixel; i++) {
                    const int p_idx = idxs[idx + i];
                    const float px_ndc = points[p_idx*3 + 0];
                    const float py_ndc = points[p_idx*3 + 1];
                    const float pz     = points[p_idx*3 + 2];
                    if (pz < 0)
                        // Don't render points behind the camera.
                        continue;

                    const float alpha = alphas[p_idx];

                    const float dx = NdcToPix(px_ndc, W) - xi;
                    const float dy = NdcToPix(py_ndc, H) - yi;
                    const float dist2 = dx*dx + dy*dy;
                    // Trim it to a circle
                    if (dist2 > radius2)
                        continue;

                    const float inv_cov00 = inv_cov[p_idx*4 + 0];
                    const float inv_cov01 = inv_cov[p_idx*4 + 1];
                    const float inv_cov10 = inv_cov[p_idx*4 + 2];
                    const float inv_cov11 = inv_cov[p_idx*4 + 3];

                    const float power = (-1.0/2.0)*(inv_cov00*dx*dx + (inv_cov01+inv_cov10)*dx*dy + inv_cov11*dy*dy);
                    if (power>0.0) {
                        continue;
                    }
                    float g_w = alpha * exp(power);
                    if (g_w < 1/255.0)
                        continue;

                    if (gathered_points_idx > kMaxPointPerPixelLocal - 1) {
                        if (pz < gathered_points_z_max) {
                            gathered_points[gathered_points_idx_max].idx = p_idx;
                            gathered_points[gathered_points_idx_max].dist2 = dist2;
                            gathered_points[gathered_points_idx_max].g_w = g_w;
                            gathered_points[gathered_points_idx_max].z = pz;

                            gathered_points_z_max = -1.0;
                            for (int j=0; j<gathered_points_idx; j++) {
                                if (gathered_points[j].z > gathered_points_z_max) {
                                    gathered_points_idx_max = j;
                                    gathered_points_z_max = gathered_points[j].z;
                                }
                            }
                        }
                    }
                    else {
                        if (pz > gathered_points_z_max) {
                            gathered_points_idx_max = gathered_points_idx;
                            gathered_points_z_max = pz;
                        }
                        gathered_points[gathered_points_idx].idx = p_idx;
                        gathered_points[gathered_points_idx_max].dist2 = dist2;
                        gathered_points[gathered_points_idx].g_w = g_w;
                        gathered_points[gathered_points_idx].z = pz;
                        gathered_points_idx++;
                    }
                }
            }
        }
        if (gathered_points_idx > 1)
        {
            QuickSortIterative(gathered_points, 0, gathered_points_idx - 1);
        }
        // BubbleSort(gathered_points, gathered_points_idx);

        //==================================================
        // slow version starts here
        //==================================================
        if (!fast_version)
        {
            // Now we have all points(dists2, idx, z) in-order of z for pixel yi,xi
            // for each color every point needs to go the the grad_buffer and add it's contribution to
            // it's index.
            float w[kMaxPointPerPixelLocal];
            float alpha_cum[kMaxPointPerPixelLocal];
            float cum_alpha = 1.0;
            int num_points_contribute = 0;

            for (int k=0; k<gathered_points_idx; k++) {
                float g_w = gathered_points[k].g_w;
                w[k] = g_w;
                alpha_cum[k] = cum_alpha;
                cum_alpha = cum_alpha * (1 - g_w);
                num_points_contribute = k+1;
                if (cum_alpha<0.001) {
                    break;
                }
            }

            for (int k=0; k < num_points_contribute; k++) {
                const float grad_out_alpha_map_f  = grad_out_alpha_map[yi*W + xi];

                const float dx = (NdcToPix(points[gathered_points[k].idx*3 + 0], W) - xi);
                const float dy = (NdcToPix(points[gathered_points[k].idx*3 + 1], H) - yi);
                const float inv_cov00 = inv_cov[gathered_points[k].idx*4 + 0];
                const float inv_cov10 = inv_cov[gathered_points[k].idx*4 + 1];
                const float inv_cov01 = inv_cov[gathered_points[k].idx*4 + 2];
                const float inv_cov11 = inv_cov[gathered_points[k].idx*4 + 3];
                const float alpha = alphas[gathered_points[k].idx];

                const float d_wk_dx = -(w[k]/2.0)*(2*inv_cov00*dx + (inv_cov10+inv_cov01)*dy); // dw_dpx-ish
                const float d_wk_dy = -(w[k]/2.0)*(2*inv_cov11*dy + (inv_cov10+inv_cov01)*dx); // dw_dpy-ish
                const float d_dx_x = W/2.0;
                const float d_dy_y = H/2.0;
                const float d_wk_z = 0.0;
                const float d_wk_inv_cov00 = -(w[k]*dx*dx)/2.0; //dw_dsigma00
                const float d_wk_inv_cov01 = -(w[k]*dx*dy)/2.0; //dw_dsigma01
                const float d_wk_inv_cov10 = d_wk_inv_cov01;    //dw_dsigma10
                const float d_wk_inv_cov11 = -(w[k]*dy*dy)/2.0; // dw_dsigma11
                const float d_wk_alpha = w[k]/alpha;

                float d_bN_c = w[k]*alpha_cum[k];

                float dA_dw = -cum_alpha/(1-w[k]);

                atomicAdd(&(grad_points[gathered_points[k].idx*3 + 0]), d_dx_x*d_wk_dx*dA_dw*grad_out_alpha_map_f);
                atomicAdd(&(grad_points[gathered_points[k].idx*3 + 1]), d_dy_y*d_wk_dy*dA_dw*grad_out_alpha_map_f);
                atomicAdd(&(grad_points[gathered_points[k].idx*3 + 2]), d_wk_z*dA_dw*grad_out_alpha_map_f);

                atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 0]), d_wk_inv_cov00*dA_dw*grad_out_alpha_map_f);
                atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 1]), d_wk_inv_cov01*dA_dw*grad_out_alpha_map_f);
                atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 2]), d_wk_inv_cov10*dA_dw*grad_out_alpha_map_f);
                atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 3]), d_wk_inv_cov11*dA_dw*grad_out_alpha_map_f);

                atomicAdd(&(grad_alpha[gathered_points[k].idx]), d_wk_alpha*dA_dw*grad_out_alpha_map_f);

                float accum_prod_1 = 1.0;
                for (int j=0; j<k; j++) {
                        accum_prod_1 *= (1 - w[j]);
                }
                for (int ch=0; ch<C; ch++) {
                    float grad_out_color_f  = grad_out_color[ch*H*W + yi*W + xi];
                    float c_k = colors[gathered_points[k].idx*C + ch];
                    float accum_sum = 0;
                    for (int u=k+1; u < num_points_contribute; u++) { // here is the quadratic part!
                        float c_u = colors[gathered_points[u].idx*C + ch];
                        float accum_prod_2 = 1.0;
                        for (int j=0; j < u; j++) {
                            if (j==k) continue;
                            accum_prod_2 *= (1 - w[j]);
                        }
                        accum_sum += c_u*w[u]*accum_prod_2;
                    }
                    float d_bN_w = c_k*accum_prod_1 - accum_sum;
                    atomicAdd(&(grad_points[gathered_points[k].idx*3 + 0]), d_dx_x*d_wk_dx*d_bN_w*grad_out_color_f);
                    atomicAdd(&(grad_points[gathered_points[k].idx*3 + 1]), d_dy_y*d_wk_dy*d_bN_w*grad_out_color_f);
                    atomicAdd(&(grad_points[gathered_points[k].idx*3 + 2]), d_wk_z*d_bN_w*grad_out_color_f);

                    atomicAdd(&(grad_colors[gathered_points[k].idx*C + ch]), d_bN_c*grad_out_color_f);

                    atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 0]), d_wk_inv_cov00*d_bN_w*grad_out_color_f);
                    atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 1]), d_wk_inv_cov01*d_bN_w*grad_out_color_f);
                    atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 2]), d_wk_inv_cov10*d_bN_w*grad_out_color_f);
                    atomicAdd(&(grad_inv_cov[gathered_points[k].idx*4 + 3]), d_wk_inv_cov11*d_bN_w*grad_out_color_f);

                    atomicAdd(&(grad_alpha[gathered_points[k].idx]), d_wk_alpha*d_bN_w*grad_out_color_f);
                }

            }
        }

        //==================================================
        // fast version starts here
        //==================================================
        else
        {
            // 1. go forward to compute and output dc/dc_i, and to compute and store B's

            float B[kMaxPointPerPixelLocal];
            int num_points_contribute = gathered_points_idx;
            float accum_prod = 1.0;
            
            for (int rpId=0; rpId < gathered_points_idx; rpId++)
            {
                const int p_idx = gathered_points[rpId].idx;
                
                // get positions
                const float px = points[3 * p_idx + 0];
                const float py = points[3 * p_idx + 1];
                const float dx = NdcToPix(px, W) - xi;
                const float dy = NdcToPix(py, H) - yi;
                
                // get shape & alpha
                const float cov00 = inv_cov[4 * p_idx + 0];
                const float cov01 = inv_cov[4 * p_idx + 1];
                const float cov10 = inv_cov[4 * p_idx + 2];
                const float cov11 = inv_cov[4 * p_idx + 3];
                const float alpha = alphas[p_idx];

                const float power = -0.5 * (cov00 * dx * dx + (cov01 + cov10) * dx * dy + cov11 * dy * dy);
                const float gauss = exp(power);
                const float w = alpha * gauss;

                // output color gradients
                const float dcdc = w * accum_prod;

                for (int ch = 0; ch < C; ch++)
                {
                    const float grad_out_color_ch = grad_out_color[ch * H * W + yi * W + xi];
                    atomicAdd(&(grad_colors[p_idx * C + ch]), dcdc * grad_out_color_ch);
                }

                accum_prod *= 1. - w;
                B[rpId] = accum_prod;
                
                if (accum_prod < 0.001) {
                    num_points_contribute = rpId + 1;
                    break;
                }
                
            }

            // 2. go backward to compute dc/da, dc/dG, dc/dp, and dc/dsigma

            float accum_rec[kMaxChannels];
            float prev_c[kMaxChannels];
            float prev_w;

            const float dxdDelx = 0.5 * W;
            const float dxdDely = 0.5 * H;

            for (int idx = num_points_contribute - 1; idx >= 0; idx--)
            {
                const int p_idx = gathered_points[idx].idx;
                
                // get point data
                const float px = points[3 * p_idx + 0];
                const float py = points[3 * p_idx + 1];
                const float dx = NdcToPix(px, W) - xi;
                const float dy = NdcToPix(py, H) - yi;

                const float cov00 = inv_cov[4 * p_idx + 0];
                const float cov01 = inv_cov[4 * p_idx + 1];
                const float cov10 = inv_cov[4 * p_idx + 2];
                const float cov11 = inv_cov[4 * p_idx + 3];

                const float power = -0.5 * (cov00 * dx * dx + (cov01 + cov10) * dx * dy + cov11 * dy * dy);                
                const float g = exp(power);

                const float a = alphas[p_idx];

                // recursive computation of dc/dw
                float dcdw = 0.;
                for (int ch = 0; ch < C; ch++)
                {
                    const float c = colors[p_idx * C + ch];
                    
                    if (idx == num_points_contribute - 1) // base case
                    {
                        accum_rec[ch] = 0;
                    }
                    else // recursion
                    {
                        accum_rec[ch] = prev_w * prev_c[ch] + (1 - prev_w) * accum_rec[ch];
                    }
                    prev_c[ch] = c;
                    
                    const float grad_out_color_ch = grad_out_color[ch * H * W + yi * W + xi];
                    dcdw += (c - accum_rec[ch]) * grad_out_color_ch;
                }
                prev_w = a * g;
                
                if (idx > 0) dcdw *= B[idx-1];

                const float grad_out_alpha_map_f = grad_out_alpha_map[yi * W + xi];
                const float dadw = -B[num_points_contribute - 1] / (1. - prev_w) * grad_out_alpha_map_f;
                const float dadGPlusdcdG = a * (dcdw + dadw);

                // dc/dp & da/dp
                const float dGdpx = -0.5 * g * (2. * cov00 * dx + (cov01 + cov10) * dy);
                const float dGdpy = -0.5 * g * (2. * cov11 * dy + (cov01 + cov10) * dx);

                atomicAdd(&grad_points[3 * p_idx + 0], dxdDelx * dGdpx * dadGPlusdcdG);
                atomicAdd(&grad_points[3 * p_idx + 1], dxdDely * dGdpy * dadGPlusdcdG);

                // dc/dsigma & da/dsigma
                const float dGdsigma00 = -0.5 * g * dx * dx;
                const float dGdsigma01 = -0.5 * g * dx * dy;
                const float dGdsigma11 = -0.5 * g * dy * dy;

                atomicAdd(&(grad_inv_cov[4 * p_idx + 0]), dGdsigma00 * dadGPlusdcdG);
                atomicAdd(&(grad_inv_cov[4 * p_idx + 1]), dGdsigma01 * dadGPlusdcdG);
                atomicAdd(&(grad_inv_cov[4 * p_idx + 2]), dGdsigma01 * dadGPlusdcdG);
                atomicAdd(&(grad_inv_cov[4 * p_idx + 3]), dGdsigma11 * dadGPlusdcdG);

                // dc/da & da/da
                atomicAdd(&(grad_alpha[p_idx]), g * (dcdw + dadw));
            }

        }
    }
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
 RasterizePointsBackwardCuda(
    const torch::Tensor& points, // (P, 3)
    const torch::Tensor& colors, // (P, C)
    const torch::Tensor& alphas, // (P)
    const torch::Tensor& inv_cov, // (P, 4)
    const int max_radius,
    const torch::Tensor& idxs, // (N, H, W, K)
    const torch::Tensor& k_idxs,
    const float znear,
    const float zfar,
    const float gamma,
    const torch::Tensor& grad_out_color,
    const torch::Tensor& grad_out_alpha_map,
    const bool fast_version) {
  const int P = points.size(0);
  const int C = colors.size(1);
  const int N = idxs.size(0);
  const int H = idxs.size(1);
  const int W = idxs.size(2);
  const int K = idxs.size(3);

  torch::Tensor grad_points = torch::zeros({P, 3}, points.options());
  torch::Tensor grad_colors = torch::zeros({P, C}, points.options());
  torch::Tensor grad_inv_cov = torch::zeros({P, 2, 2}, points.options());
  torch::Tensor grad_alpha = torch::zeros({P, 1}, points.options());

  const size_t blocks = 1024;
  const size_t threads = 64;
  RasterizePointsBackwardCudaKernel<<<blocks, threads>>>(
      points.contiguous().data<float>(),
      colors.contiguous().data<float>(),
      alphas.contiguous().data<float>(),
      inv_cov.contiguous().data<float>(),
      max_radius,
      idxs.contiguous().data<int32_t>(),
      k_idxs.contiguous().data<int32_t>(),
      N,
      P,
      C,
      H,
      W,
      K,
      znear,
      zfar,
      gamma,
      grad_out_color.contiguous().data<float>(),
      grad_out_alpha_map.contiguous().data<float>(),
      grad_points.contiguous().data<float>(),
      grad_colors.contiguous().data<float>(),
      grad_inv_cov.contiguous().data<float>(),
      grad_alpha.contiguous().data<float>(),
      fast_version);

  return std::make_tuple(grad_points, grad_colors, grad_inv_cov, grad_alpha);
}
