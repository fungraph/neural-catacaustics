#pragma once
#include <torch/extension.h>
#include <cstdio>
#include <tuple>

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePointsGKCuda(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& alphas,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const int image_height,
    const int image_width,
    const float zfar,
    const float znear,
    const float gamma);

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
 RasterizePointsBackwardCuda(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& alphas,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const torch::Tensor& idxs,
    const torch::Tensor& k_idxs,
    const float znear,
    const float zfar,
    const float gamma,
    const torch::Tensor& grad_out_color,
    const torch::Tensor& grad_out_alpha_map,
    const bool fast_version);


std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePointsBackward(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& alphas,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const torch::Tensor& idxs,
    const torch::Tensor& k_idxs,
    const float znear,
    const float zfar,
    const float gamma,
    const torch::Tensor& grad_out_color,
    const torch::Tensor& grad_out_alpha_map,
    const bool fast_version) {
  if (points.type().is_cuda()) {
    return RasterizePointsBackwardCuda(points, colors, alphas,
                                       inv_cov, max_radius,
                                       idxs, k_idxs, znear, zfar,
                                       gamma, grad_out_color, grad_out_alpha_map,
                                       fast_version);
  } else {
    AT_ERROR("No CPU support");
  }
}

std::tuple<torch::Tensor, torch::Tensor, torch::Tensor, torch::Tensor>
RasterizePoints(
    const torch::Tensor& points,
    const torch::Tensor& colors,
    const torch::Tensor& alphas,
    const torch::Tensor& inv_cov,
    const int max_radius,
    const int image_height,
    const int image_width,
    const float zfar,
    const float znear,
    const float gamma)
{
    return RasterizePointsGKCuda(
        points,
        colors,
        alphas,
        inv_cov,
        max_radius,
        image_height,
        image_width,
        zfar,
        znear,
        gamma);
}
