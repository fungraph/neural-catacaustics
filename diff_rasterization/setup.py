from setuptools import setup
from torch.utils.cpp_extension import CUDAExtension, BuildExtension

setup(
    name="diff_rast",
    ext_modules=[
        CUDAExtension(
            name="diff_rasterization._C",
            sources=["rasterize_points.cu", "ext.cpp"],
            extra_compile_args={"nvcc": [], "cxx": []})
        ],
    cmdclass={
        'build_ext': BuildExtension
    }
)
