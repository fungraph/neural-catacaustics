#pragma once

// Given a pixel coordinate 0 <= i < S, convert it to a normalized device
// coordinate in the range [-1, 1]. We divide the NDC range into S evenly-sized
// pixels, and assume that each pixel falls in the *center* of its range.
__device__ inline float PixToNdc(int i, int S) {
  // NDC x-offset + (i * pixel_width + half_pixel_width)
  return -1 + (2 * i + 1.0f) / S;
}

__device__ inline float NdcToPix(float i, int S) {
  return ((i + 1.0)*S - 1.0)/2.0;
}

__host__ inline int ceil(int x, int y) {
    return (x + y - 1) / y;
}

// The maximum number of points per pixel that we can return. Since we use
// thread-local arrays to hold and sort points, the maximum size of the array
// needs to be known at compile time. There might be some fancy template magic
// we could use to make this more dynamic, but for now just fix a constant.
// TODO: is 8 enough? Would increasing have performance considerations?
const int32_t kMaxPointsPerPixel = 550; //550;
const int32_t kMaxPointPerPixelLocal = 550;

const int32_t kMaxChannels = 32;

#define TILE_SIZE 16
#define TILE_STORAGE 50000 // TODO: pretty high...

template <typename T>
__device__ inline void BubbleSort(T* arr, int n) {
  bool already_sorted;
  // Bubble sort. We only use it for tiny thread-local arrays (n < 8); in this
  // regime we care more about warp divergence than computational complexity.
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      if (arr[j + 1] < arr[j]) {
        already_sorted = false;
        T temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}

__device__ inline void BubbleSort2(int32_t* arr, const float* points, int n) {
  bool already_sorted;
  // Bubble sort. We only use it for tiny thread-local arrays (n < 8); in this
  // regime we care more about warp divergence than computational complexity.
  for (int i = 0; i < n - 1; ++i) {
    already_sorted=true;
    for (int j = 0; j < n - i - 1; ++j) {
      float p_j0_z = points[arr[j]*3 + 2];
      float p_j1_z = points[arr[j+1]*3 + 2];
      if (p_j1_z < p_j0_z) {
        already_sorted = false;
        int32_t temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
    if (already_sorted)
        break;
  }
}

//===================================================

// recursive quick sort - stack gets too deep for big arrays
// template <typename T>
// __device__ inline void QuickSort(T* arr, int left, int right)
// {
//   int i = left;
//   int j = right;
//   T pivot = arr[int((left + right) / 2)];
//   T temp;
//   while (i <= j)
//   {
//     while (arr[i] < pivot) i++;
//     while (pivot < arr[j]) j--;
//     if (i <= j)
//     {
//       temp = arr[i];
//       arr[i] = arr[j];
//       arr[j] = temp;
//       i++;
//       j--;
//     }
//   }
//   if (left < j) QuickSort(arr, left, j);
//   if (i < right) QuickSort(arr, i, right);
// }

//===================================================

// iterative quick sort
template <typename T>
__device__ inline void QuickSortIterative(T* arr, int left, int right)
{
    int stack[kMaxPointPerPixelLocal];
    int top = -1;
    T x, temp;

    stack[++top] = left;
    stack[++top] = right;
 
    while (top >= 0) 
    {
        // pop
        right = stack[top--];
        left = stack[top--];

        // partition
        x = arr[right];
        int i = left - 1;
        for (int j = left; j <= right - 1; j++) 
        {
            if (arr[j] <= x) 
            {
                i++;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        temp = arr[i+1];
        arr[i+1] = arr[right];
        arr[right] = temp;
        int p = i + 1;

        // push
        if (p - 1 > left)
        {
            stack[++top] = left;
            stack[++top] = p - 1;
        }
        if (p + 1 < right) 
        {
            stack[++top] = p + 1;
            stack[++top] = right;
        }
    }
}