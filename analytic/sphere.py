import os, sys
import numpy as np
import math
from scipy.optimize import root
import matplotlib.pyplot as plt
import open3d as o3d

def getDir():
    return os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

sys.path.append(getDir())

from utils.graphics_utils import create_quad_plane_mesh

#========================================

def circle(pos, radius):
    def curve(t):
        t_full = t * 2 * math.pi
        x = radius * np.sin(t_full) + pos[0]
        y = radius * np.cos(t_full) + pos[1]
        return np.array([x, y])
    return curve

#========================================

def circle_catacaustic(pos, radius, point_pos):
    
    # create curve in canonical configuration: 
    # circle center is (0,0) with unit radius, and reflected point lies on x-axis
    eps = 1e-10
    radius -= eps
    diff = pos - point_pos
    dst_norm = -np.linalg.norm(diff) / radius
    assert abs(dst_norm) > 1, "Reflected point lies inside the circle"
    
    def canonical_curve(t):
        x = dst_norm * (1 - 3 * dst_norm * np.cos(t) + 2 * dst_norm * np.cos(t) ** 3) / (-(1 + 2 * dst_norm ** 2) + 3 * dst_norm * np.cos(t))
        y = (2 * dst_norm ** 2 * np.sin(t) ** 3) / (1 + 2 * dst_norm ** 2 - 3 * dst_norm * np.cos(t))
        return np.array([x,y])

    # only consider the part of the curve between the two points that touch the circle
    def objective(t):
        p = canonical_curve(t)
        return np.linalg.norm(p) - 1

    t_start = root(objective, math.pi - 1).x
    t_end   = root(objective, math.pi + 1).x    

    # transform canonical curve to final configuration with normalized parameters
    angle = math.atan2(diff[1], diff[0])
    rot_matrix = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])

    def curve(t):
        t_interval = t * t_end + (1 - t) * t_start
        p = canonical_curve(t_interval)
        p = radius * rot_matrix.dot(p).T + pos
        return p.T
    
    return curve

#========================================

# rotation matrix that rotates a onto b
def rodriguez_rotation(b, a):
    v = np.cross(a, b)
    c = a.dot(b)
    vx = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    return np.eye(3) + vx + 1 / (1+c) * vx.dot(vx)

#========================================

def sphere_catacaustic(sphere_center, sphere_radius, point_pos, catacaustic_res):

    # create 1D curve in canonical configuration: 
    # circle center is (0,0) with unit radius, and reflected point lies on x-axis
    eps = 1e-10
    sphere_radius -= eps
    diff = sphere_center - point_pos
    dst = np.linalg.norm(diff)
    dst_norm = dst / sphere_radius
    assert dst_norm > 1, "Reflected point lies inside the circle"
    
    def canonical_curve(t):
        d = -dst_norm
        x = d * (1 - 3 * d * np.cos(t) + 2 * d * np.cos(t) ** 3) / (-(1 + 2 * d ** 2) + 3 * d * np.cos(t))
        y = (2 * d ** 2 * np.sin(t) ** 3) / (1 + 2 * d ** 2 - 3 * d * np.cos(t))
        return np.array([x,y]).T

    # only consider the part of the curve until it touches the circle
    def objective(t):
        p = canonical_curve(t)
        return np.linalg.norm(p) - 1

    t_start = math.pi  # cusp
    t_end = root(objective, math.pi + 1).x  # touching point
    
    # find global rotation to orient the surface
    global_rot = rodriguez_rotation(diff / dst, np.array([1, 0, 0]))

    def surface(u, v):

        def expand_rotation(r):
            return np.tile(r, (v.shape[0], 1, 1))

        def apply_rotation(r, p):
            return np.einsum('abc,ac->ab', r, p)    

        # sample canonical curve
        t_interval = u * t_end + (1 - u) * t_start
        p = canonical_curve(t_interval)
        p = np.pad(p, ((0,0),(0,1)), 'constant', constant_values=0)
        
        # create body of revolution around x-axis
        angle = 2 * math.pi * v
        rot_x = expand_rotation(np.eye(3))
        rot_x[:,1,1] = np.cos(angle)
        rot_x[:,1,2] = -np.sin(angle)
        rot_x[:,2,1] = np.sin(angle)
        rot_x[:,2,2] = rot_x[:,1,1]        
        q = apply_rotation(rot_x, p)
        
        # transform from canonical to actual coordinates
        return sphere_radius * apply_rotation(expand_rotation(global_rot), q) + sphere_center

    vertices_2d, tris = create_quad_plane_mesh(catacaustic_res)
    vertices_3d = surface(vertices_2d[:, 1], vertices_2d[:,0])

    return vertices_3d, tris

#========================================

def main():
    
    # parameters
    sphere_center = np.array([5, -1, 0])
    sphere_radius = 2.
    point_pos = np.array([5, 1, 1])
    sphere_res = 20
    catacaustic_res = [30, 30]

    # catacaustic surface
    vertices, tris = sphere_catacaustic(sphere_center, sphere_radius, point_pos, catacaustic_res)
    catacaustic_mesh = o3d.geometry.TriangleMesh()
    catacaustic_mesh.vertices = o3d.utility.Vector3dVector(vertices)
    catacaustic_mesh.triangles = o3d.utility.Vector3iVector(tris)

    # visualization
    sphere = o3d.geometry.TriangleMesh.create_sphere(radius=sphere_radius, resolution=sphere_res).translate(*sphere_center[None, ...])
    point = o3d.geometry.TriangleMesh.create_sphere(radius=0.1).translate(*point_pos[None, ...])
    #viz_meshes = [sphere, point, catacaustic_mesh]
    viz_meshes = [point, catacaustic_mesh]

    o3d.visualization.draw_geometries(viz_meshes, mesh_show_wireframe=True, mesh_show_back_face=True)

    return


    #----------------------------------------------
    # 2D part starts here

    pos = np.array([0, 0], dtype=np.float32)
    radius = 2
    point_pos = np.array([4, 3], dtype=np.float32)
    
    circle_curve = circle(pos, radius)
    catacaustic_curve = circle_catacaustic(pos, radius, point_pos)

    t = np.linspace(0, 1, 100)
    circle_samples = circle_curve(t)
    cata_samples = catacaustic_curve(t)

    #-----------------------------
    # plotting
    fig, ax = plt.subplots(figsize=(6, 6))
    ax.plot(*point_pos, marker="o")
    ax.plot(*circle_samples)
    ax.plot(*cata_samples)
    plt.xlim([-5, 5])
    plt.ylim([-5, 5])
    plt.show()

#========================================

if __name__ == "__main__":
    main()
    print("=== TERMINATED ===")
