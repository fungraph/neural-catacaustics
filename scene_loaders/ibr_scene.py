import os
import math
import sys
import torch
from torch import nn
import numpy as np
import random
from model_defs.ResNet_2DConv import FixUpResNet_mixed, FixUpResNet_withDir, FixUpResNet_withDir_DiffuseOnly
from utils.system_utils import mkdir_p, searchForMaxIteration
from utils.general_utils import batchify, patchify, sample_mesh_pytorch3d, sample_mesh_o3d
from scene_loaders.read_scenes_types import sceneLoadTypeCallbacks
import open3d as o3d
from model_defs.warp_field_mlp import BasicMLP, TwoBlockMLP, OneBlockMLP, NormalizedMLP, CatacausticMLP, NormalizedMLP_SE3, ProgressiveCatacausticMLP
from scene_loaders.point_cloud import PointCloud
import torchvision.transforms.functional as TF
from utils.graphics_utils import geom_transform_vectors
from utils.camera_utils import cameraList_from_camInfos
from utils.graphics_utils import geom_transform_points

from utils.general_utils import make_background_from_another_pc

def randomizeNeighbors(neighbors, N):
    return np.array(neighbors)[np.random.rand(len(neighbors)) > 1.0 / 20.0][:N]

class Sphere(nn.Module):
    def __init__(self, radius, extra_features):
        super(Sphere, self).__init__()
        self.texture = nn.Parameter(0.5*torch.ones((1, 3+extra_features, 400, 1000), device="cuda"))
        self.radius2 = torch.tensor([radius*radius]).cuda()
        self.center = torch.zeros((3)).cuda()

    def intersection(self, ray_o, ray_d):
        L = self.center - ray_o
        tca = L.repeat((ray_d.shape[0],1)).unsqueeze(-2).bmm(ray_d.unsqueeze(-1)).squeeze(-1)
        d2 = L.unsqueeze(-2).bmm(L.unsqueeze(-1)).squeeze(-1) - tca * tca
        thc = torch.sqrt(self.radius2 - d2)
        t0 = tca - thc
        t1 = tca + thc

        return ray_o + t1*ray_d

    def getRayTexture(self, camera, patch):

        dirrays = camera.getDirRays3()[:,:, patch[1]:patch[1]+patch[3], patch[0]:patch[0]+patch[2]]

        batched_rays, rays_shape = batchify(dirrays)
        intersections = self.intersection(camera.camera_center,
                                          batched_rays)
        intersections_patch = patchify(intersections, rays_shape)
        dirs = intersections_patch/intersections_patch.norm(2, dim=1, keepdim=True)

        theta = torch.acos(dirs[:,2:3,...])/(math.pi/2.0) - 1.0
        phi = torch.atan2(dirs[:,1:2,...], dirs[:,0:1,...])/math.pi - 1.0

        grid = torch.cat((phi, -theta), dim=1).permute(0,2,3,1)
        projected_sphere_texture = torch.nn.functional.grid_sample(self.texture, grid,
                                                           mode="bilinear", padding_mode="reflection",
                                                           align_corners=False)

        return projected_sphere_texture


class Scene:
    def __init__(self, args, resolution_scales=None, load_input_data=True, load_point_clouds=True, shuffle=True):
        """b
        :param path: Path to colmap scene main folder.
        """
        print("Loading Scene from: {}".format(args.input_path))
        self.bg_type = args.background
        self.diffuse_only = args.diffuse_only

        if args.load_iter or args.scene_representation_folder:
            if args.load_iter:
                args.load_iter = args.load_iter
            else:
                args.load_iter = searchForMaxIteration(os.path.join(args.scene_representation_folder, "diffuse_point_cloud"))
            print("Loading iteration {}".format(args.load_iter))
        self.load_iter = args.load_iter
        self.train_cameras = {}
        self.validation_cameras = {}

        self.map_imgfilename_to_idx = {}
        if os.path.exists(os.path.join(args.input_path, "colmap")):
            scene_info = sceneLoadTypeCallbacks["Colmap"](args.input_path, NerfNormalize=args.nerfpp_normalize)
        elif os.path.exists(os.path.join(args.input_path, "meshes")):
            scene_info = sceneLoadTypeCallbacks["RealityCapture"](args.input_path, NerfNormalize=args.nerfpp_normalize)
        elif os.path.exists(os.path.join(args.input_path, "scene.xml")):
            scene_info = sceneLoadTypeCallbacks["Mitsuba"](args.input_path, NerfNormalize=args.nerfpp_normalize)
        else:
            assert False, "Could not recognize scene type!"
        if shuffle:
            random.shuffle(scene_info.train_cameras)  # Multi-res consistent random shuffling
            random.shuffle(scene_info.validation_cameras)  # Multi-res consistent random shuffling

        for resolution_scale in resolution_scales:
            if load_input_data:
                print("Loading Training Cameras")
                self.train_cameras[resolution_scale] = cameraList_from_camInfos(scene_info.train_cameras,
                                                                                resolution_scale, args,
                                                                                polytope_folder="train_polytope_masks")
                print("Loading Validation Cameras")
                self.validation_cameras[resolution_scale] = cameraList_from_camInfos(scene_info.validation_cameras,
                                                                                     resolution_scale, args,
                                                                                     polytope_folder="validation_polytope_masks")

        self.use_mask = not args.no_mask
        self.specular_point_cloud = PointCloud(extra_features=args.extra_features)
        self.diffuse_point_cloud = PointCloud(extra_features=args.extra_features + 1)
        self.background_point_cloud = PointCloud(extra_features=args.extra_features)

        self.warp_field_mlp = None
        if args.load_iter:
            self.diffuse_point_cloud.load_ply(os.path.join(args.scene_representation_folder,
                                                       "diffuse_point_cloud",
                                                       "iteration_" + str(args.load_iter),
                                                       "point_cloud.ply"),
                                              active_normals=True)
            if not self.diffuse_only:
                self.specular_point_cloud.load_ply(os.path.join(args.scene_representation_folder,
                                                   "specular_point_cloud", "iteration_" + str(args.load_iter),
                                                   "point_cloud.ply"),
                                                   active_normals=False)
                self.warp_field_mlp = torch.load(os.path.join(args.scene_representation_folder, "warp_field",
                                                                "iteration_" + str(args.load_iter),
                                                                "warp_field_mlp.ts")).cuda()
            else:
                # Create an arbitrary warp field MLP
                # TODO: find a way to do this more properly
                self.warp_field_mlp = NormalizedMLP(int(args.warp_mlp_width), int(args.warp_mlp_depth),
                                    torch.zeros(1, 3),
                                    torch.ones(1, 3),
                                    torch.zeros(1, 3),
                                    torch.ones(1, 3),).cuda()
            self.view_renderer = torch.load(os.path.join(args.scene_representation_folder, "neural_renderers",
                                                             "iteration_" + str(args.load_iter),
                                                             "view_renderer.ts")).cuda()
            self.environment_map = torch.load(os.path.join(args.scene_representation_folder, "environment_map",
                                                             "iteration_" + str(args.load_iter),
                                                             "environment_map.ts")).cuda()
        else:
            if args.experimental_render:
                self.view_renderer = FixUpResNet_mixed(in_channels=2 * (args.extra_features + 3),
                                                       out_channels=3,
                                                       internal_depth=32,
                                                       blocks=6,
                                                       kernel_size=args.kernel_size).cuda()
            else:
                if self.diffuse_only:
                    self.view_renderer = FixUpResNet_withDir_DiffuseOnly(in_channels=args.extra_features + 3,
                                                       out_channels=3,
                                                       internal_depth=16,
                                                       blocks=6,
                                                       kernel_size=args.kernel_size).cuda()
                else:
                    self.view_renderer = FixUpResNet_withDir(in_channels=2*(args.extra_features + 3),
                                                    out_channels=3,
                                                    internal_depth=32,
                                                    blocks=6,
                                                    kernel_size=args.kernel_size).cuda()


            self.polytope_mean = np.array([0.0, 0.0, 0.0])
            self.polytope_max = np.array([1.0, 1.0, 1.0])
            self.polytope_std = np.array([1.0, 1.0, 1.0])
            if os.path.isfile(os.path.join(args.input_path, "polytope.obj")):
                polytope = o3d.io.read_triangle_mesh(os.path.join(args.input_path, "polytope.obj"))
                self.polytope_mean = np.asarray(polytope.vertices).mean(axis=0)
                self.polytope_max = np.asarray(np.abs(polytope.vertices)).max(axis=0)
                self.polytope_std = np.asarray(polytope.vertices).std(axis=0)
                print("polytope STD: ", self.polytope_std)
            #mean, std = self.test_func(self.warp_field_mlp)


            cam_distribution = []
            for cam in self.train_cameras[next(iter(self.train_cameras))]:
                cam_distribution.append(cam.camera_center)
            cam_dist = torch.stack(cam_distribution)
            #cam_dist = torch.tensor([0.1, 0.5])

            # print("cameras distribution : ", cam_dist.shape, cam_dist.mean(dim=0), cam_dist.std(dim=0))

            if args.lambda_cata > 0.0:
                if args.progressive_catacaustics:
                    self.warp_field_mlp = ProgressiveCatacausticMLP(width_param=args.warp_mlp_width,
                                                    width_embed=args.warp_mlp_width,
                                                    depth_param=args.warp_mlp_depth,
                                                    depth_embed=args.warp_mlp_depth,
                                                    mean=torch.tensor(self.polytope_mean).float().cuda().unsqueeze(0),
                                                    std=torch.tensor(self.polytope_std).float().cuda().unsqueeze(0),
                                                    cam_mean=cam_dist.mean(dim=0), cam_std=cam_dist.std(dim=0)).cuda()
                else:
                    self.warp_field_mlp = CatacausticMLP(width_param=args.warp_mlp_width,
                                                        width_embed=args.warp_mlp_width,
                                                        depth_param=args.warp_mlp_depth,
                                                        depth_embed=args.warp_mlp_depth,
                                                        mean=torch.tensor(self.polytope_mean).float().cuda().unsqueeze(0),
                                                        std=torch.tensor(self.polytope_std).float().cuda().unsqueeze(0),
                                                        cam_mean=cam_dist.mean(dim=0), cam_std=cam_dist.std(dim=0)).cuda()
            else:
                if args.se3_warp:
                    self.warp_field_mlp = NormalizedMLP_SE3(int(args.warp_mlp_width), int(args.warp_mlp_depth),
                                                            torch.tensor(self.polytope_mean).float().cuda().unsqueeze(
                                                                0),
                                                            torch.tensor(self.polytope_std).float().cuda().unsqueeze(0),
                                                            cam_dist.mean(dim=0), cam_dist.std(dim=0)).cuda()
                else:
                    self.warp_field_mlp = NormalizedMLP(int(args.warp_mlp_width), int(args.warp_mlp_depth),
                                                        torch.tensor(self.polytope_mean).float().cuda().unsqueeze(0),
                                                        torch.tensor(self.polytope_std).float().cuda().unsqueeze(0),
                                                        cam_dist.mean(dim=0), cam_dist.std(dim=0)).cuda()

            # Diffuse PC initialisation
            if load_point_clouds:
                scene_info.point_cloud.paint_uniform_color(np.array([0.5, 0.5, 0.5]))
                self.diffuse_point_cloud.initialize_with_pcd(scene_info.point_cloud,
                                                             lr_xyz=args.diffuse_xyz_lr,
                                                             lr_feat=args.feature_lr,
                                                             lr_alpha=args.alpha_lr,
                                                             lr_u=args.diffuse_uncertainty_lr,
                                                             lr_normal=args.normal_lr,
                                                             apply_lr=False,
                                                             every_kth_point=round(4**2.5),
                                                             normals=(not args.no_diffuse_pc_normals))
                if not self.diffuse_only:
                    try:
                        polytope_pc = sample_mesh_pytorch3d(os.path.join(args.input_path, "polytope.obj"),
                                                            int(args.total_specular_points))
                    except:
                        polytope_pc = sample_mesh_o3d(os.path.join(args.input_path, "polytope.obj"),
                                                      int(args.total_specular_points))
                    self.specular_point_cloud.initialize_with_pcd(polytope_pc,
                                                                  lr_xyz=args.specular_xyz_lr,
                                                                  lr_feat=args.feature_lr,
                                                                  lr_alpha=args.alpha_lr,
                                                                  lr_u=args.specular_uncertainty_lr,
                                                                  lr_normal=args.normal_lr,
                                                                  apply_lr=False,
                                                                  every_kth_point=round(4**2.5),
                                                                  normals=False)

            self.environment_map = nn.Parameter(-2.0*torch.ones((1, 3+args.extra_features, 400, 1000), device="cuda"))

    def getNClosestCameras(self, ref_camera, N):
        camera_distances = []
        ref_camera_center = ref_camera.camera_center
        for idx, cam in enumerate(self.getAllCameras()):
            cam_center = cam.camera_center
            distance = torch.dist(ref_camera_center, cam_center)
            camera_distances.append((idx, distance))
        camera_distances.sort(key=lambda tup: tup[1])

        return [self.getAllCameras()[idx] for idx, distance in camera_distances][1:N+1]

    def save(self, path, iteration):
        diffuse_point_cloud_path = os.path.join(path, "diffuse_point_cloud/iteration_{}".format(iteration))
        self.diffuse_point_cloud.save_ply(os.path.join(diffuse_point_cloud_path, "point_cloud.ply"))

        if not self.diffuse_only:
            specular_point_cloud_path = os.path.join(path, "specular_point_cloud/iteration_{}".format(iteration))
            self.specular_point_cloud.save_ply(os.path.join(specular_point_cloud_path, "point_cloud.ply"))

            warp_field_path = os.path.join(path, "warp_field", "iteration_{}".format(iteration))
            mkdir_p(warp_field_path)
            torch.save(self.warp_field_mlp, os.path.join(warp_field_path, "warp_field_mlp.ts"))

        neural_renderers_path = os.path.join(path, "neural_renderers", "iteration_{}".format(iteration))
        mkdir_p(neural_renderers_path)
        #torch.save(self.mask_renderer, os.path.join(neural_renderers_path, "mask_renderer.ts"))
        torch.save(self.view_renderer, os.path.join(neural_renderers_path, "view_renderer.ts"))

        environment_map_path = os.path.join(path, "environment_map", "iteration_{}".format(iteration))
        mkdir_p(environment_map_path)
        torch.save(self.environment_map, os.path.join(environment_map_path, "environment_map.ts"))

    def normalizeNormals(self):
        self.global_normals = torch.nn.Parameter(self.global_normals / self.global_normals.norm(2, dim=1, keepdim=True))

    def getTrainCameras(self, scale=1.0):
        return self.train_cameras[scale]

    def getValidationCameras(self, scale=1.0):
        return self.validation_cameras[scale]

    def debugView2(self, scale=1.0):
        ps.init()

        ps_pc_diffuse = ps.register_point_cloud("point_cloud_diffuse", self.diffuse_point_cloud.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_diffuse.add_color_quantity("point_cloud_colors", self.diffuse_point_cloud.global_features[:,:3].detach().cpu().numpy(), enabled=True)

        ps_pc_specular = ps.register_point_cloud("point_cloud_specular", self.specular_point_cloud.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_specular.add_color_quantity("point_cloud_colors", 0.5*torch.ones((self.specular_point_cloud.global_xyz.shape[0], 3)).detach().cpu().numpy(), enabled=True)

        cameras = self.cameras[scale][:2]

        cam_poses = np.array([x.camera_center.cpu().detach().numpy() for x in cameras]).squeeze()
        ps_cameras = ps.register_point_cloud("cameras", cam_poses[0:1], enabled=True)

        dirs = [geom_transform_vectors(torch.tensor([[0.0, 0.0, 1.0]]), x.world_view_transform.inverse().cpu()).cpu().detach().numpy() for x in cameras]
        ps_cameras.add_vector_quantity("cam_dirs", np.array(dirs)[0:1].squeeze(0), enabled=True)
        """
        cam01 = np.array(self.cameras[scale][0].camera_center.cpu().detach().numpy())
        cam_poses_rand_repeat = np.repeat(cam01, 100, 0)
        ps_cameras_rand = ps.register_point_cloud("cameras_rand2", cam_poses_rand_repeat, enabled=True)
        rand_v = torch.cat((torch.randn(100, 2), torch.ones([100, 1])), dim=1)
        d_rand = geom_transform_vectors(rand_v, cameras[0].world_view_transform.inverse().cpu()).cpu().detach().numpy()
        d_rand_norm = d_rand / np.linalg.norm(d_rand, 2, axis=1, keepdims=True)
        ps_cameras_rand.add_vector_quantity("cam_dirs_rand2", d_rand_norm, enabled=True)

        cam01 = np.array(self.cameras[scale][1].camera_center.cpu().detach().numpy())
        cam_poses_rand_repeat = np.repeat(cam01, 100, 0)
        ps_cameras_rand1 = ps.register_point_cloud("cameras_rand1", cam_poses_rand_repeat, enabled=True)
        rand_v = torch.cat((torch.randn(100, 2)*0.2, torch.ones([100, 1])), dim=1)
        d_rand = geom_transform_vectors(rand_v, cameras[0].world_view_transform.inverse().cpu()).cpu().detach().numpy()
        d_rand_norm = d_rand / np.linalg.norm(d_rand, 2, axis=1, keepdims=True)
        ps_cameras_rand1.add_vector_quantity("cam_dirs_rand1", d_rand_norm, enabled=True)
        """

        #dir = -getDirRays3(self.cameras[scale][0], sigma=1.0).squeeze().view(3,-1).permute(1,0).cpu().detach().numpy()
        #cam01 = np.array(self.cameras[scale][0].camera_center.cpu().detach().numpy())
        #cam_poses_rand_repeat = np.repeat(cam01, dir.shape[0], 0)
        #ps_cameras_rand1 = ps.register_point_cloud("cameras_rand1", cam_poses_rand_repeat, enabled=True)
        #ps_cameras_rand1.add_vector_quantity("cam_dirs_rand1", dir, enabled=True)


        #ps_x = ps.register_point_cloud("origin_x", np.array([[0., 0., 0.]]), enabled=True)
        #ps_y = ps.register_point_cloud("origin_y", np.array([[0., 0., 0.]]), enabled=True)
        #ps_z = ps.register_point_cloud("origin_z", np.array([[0., 0., 0.]]), enabled=True)
        #ps_x.add_vector_quantity("dir_x", np.array([[1., 0., 0.]]), enabled=True, color=(1., 0., 0.))
        #ps_y.add_vector_quantity("dir_y", np.array([[0., 1., 0.]]), enabled=True, color=(0., 1., 0.))
        #ps_z.add_vector_quantity("dir_z", np.array([[0., 0., 1.]]), enabled=True, color=(0., 0., 1.))

        #ps.look_at(cam_poses[0], np.array(dirs).squeeze()[0])

        ps.show()

    def findCenterOfInterest(self, scale):
        """P0 and P1 are NxD arrays defining N lines.
        D is the dimension of the space. This function
        returns the least squares intersection of the N
        lines from the system given by eq. 13 in
        http://cal.cs.illinois.edu/~johannes/research/LS_line_intersect.pdf.
        https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#In_three_dimensions_2
        """
        P0 = torch.tensor([], device="cuda")
        P1 = torch.tensor([], device="cuda")
        for cam in self.getTrainCameras(scale):
            p0 = cam.camera_center.view(1, 3)
            p1 = geom_transform_points(torch.tensor([[0.0, 0.0, -1.0]], device="cuda"),
                                       cam.world_view_transform.inverse())
            P0 = torch.cat((P0, p0), dim=0)
            P1 = torch.cat((P1, p1), dim=0)
        P0 = P0.detach().cpu().numpy()
        P1 = P1.detach().cpu().numpy()

        # generate all line direction vectors
        n = (P1 - P0) / np.linalg.norm(P1 - P0, axis=1)[:, np.newaxis]  # normalized

        # generate the array of all projectors
        projs = np.eye(n.shape[1]) - n[:, :, np.newaxis] * n[:, np.newaxis]  # I - n*n.T
        # see fig. 1

        # generate R matrix and q vector
        R = projs.sum(axis=0)
        q = (projs @ P0[:, :, np.newaxis]).sum(axis=0)

        # solve the least squares problem for the
        # intersection point p: Rp = q
        p = np.linalg.lstsq(R, q, rcond=None)[0]

        return p.reshape(-1, 3)

    def debugView(self, scale=1.0):
        import polyscope as ps

        ps.init()

        ps_pc_diffuse = ps.register_point_cloud("point_cloud_diffuse", self.diffuse_point_cloud.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_diffuse.add_color_quantity("point_cloud_colors", self.diffuse_point_cloud.global_features[:,:3].detach().cpu().numpy(), enabled=True)

        ps_pc_specular = ps.register_point_cloud("point_cloud_specular", self.specular_point_cloud.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_specular.add_color_quantity("point_cloud_colors", 0.5*torch.ones((self.specular_point_cloud.global_xyz.shape[0], 3)).detach().cpu().numpy(), enabled=True)

        ps_pc_background = ps.register_point_cloud("point_cloud_background", self.background_point_cloud.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_background.add_color_quantity("point_cloud_colors", 0.5*torch.ones((self.background_point_cloud.global_xyz.shape[0], 3)).detach().cpu().numpy(), enabled=True)


        cam_poses = np.array([x.camera_center.cpu().detach().numpy() for x in self.getTrainCameras(scale)]).squeeze()
        ps_cameras = ps.register_point_cloud("cameras", cam_poses, enabled=True)

        dirs = [geom_transform_vectors(torch.tensor([[0.0, 0.0, 1.0]]), x.world_view_transform.inverse().cpu()).cpu().detach().numpy() for x in self.getTrainCameras(scale)]
        ps_cameras.add_vector_quantity("cam_dirs", np.array(dirs).squeeze(), enabled=True)

        poi = self.findCenterOfInterest(scale)
        print(poi)
        ps_poi = ps.register_point_cloud("point_of_interest", poi, enabled=True)

        ps_x = ps.register_point_cloud("origin_x", np.array([[0., 0., 0.]]), enabled=True)
        ps_y = ps.register_point_cloud("origin_y", np.array([[0., 0., 0.]]), enabled=True)
        ps_z = ps.register_point_cloud("origin_z", np.array([[0., 0., 0.]]), enabled=True)
        ps_x.add_vector_quantity("dir_x", np.array([[1., 0., 0.]]), enabled=True, color=(1., 0., 0.))
        ps_y.add_vector_quantity("dir_y", np.array([[0., 1., 0.]]), enabled=True, color=(0., 1., 0.))
        ps_z.add_vector_quantity("dir_z", np.array([[0., 0., 1.]]), enabled=True, color=(0., 0., 1.))

        #ps.look_at(cam_poses[0], np.array(dirs).squeeze()[0])

        ps.show()
