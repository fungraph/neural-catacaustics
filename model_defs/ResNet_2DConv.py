import torch.nn as nn
from model_defs.netblocs import ConvModule, FixupResidualChain
import os
import torch

class FixUpResNet(nn.Module):
    def __init__(self, in_channels, out_channels, internal_depth, blocks, kernel_size, dropout=0.0, last_activation=None):
        super(FixUpResNet, self).__init__()

        self.encoder = nn.Sequential(
                           ConvModule(in_channels, internal_depth, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )

        self.decoder = nn.Sequential(
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                           ConvModule(internal_depth, out_channels, ksize=kernel_size, pad=True, activation=last_activation, norm_layer=None, padding_mode="reflect")
                       )


    def forward(self, x):
        x_encoded = self.encoder(x)
        x_out = self.decoder(x_encoded)
        return x_out

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)

class FixUpResNet_mixed(nn.Module):
    def __init__(self, in_channels, out_channels, internal_depth, blocks, kernel_size, dropout=0.0, last_activation=None):
        super(FixUpResNet_mixed, self).__init__()

        self.encoder = nn.Sequential(
                           ConvModule(in_channels, internal_depth, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )

        self.decoder = nn.Sequential(
                            FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(1), padding_mode="reflect", dropout=dropout),
                            FixupResidualChain(internal_depth, ksize=3, depth=int(1), padding_mode="reflect", dropout=dropout),
                            ConvModule(internal_depth, out_channels, ksize=3, pad=True, activation=last_activation, norm_layer=None, padding_mode="reflect")
                       )


    def forward(self, x):
        x_encoded = self.encoder(x)
        x_out = self.decoder(x_encoded)
        return x_out

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)

class FixUpResNet_withDir(nn.Module):
    def __init__(self, in_channels, out_channels, internal_depth, blocks, kernel_size, dropout=0.0, last_activation=None):
        super(FixUpResNet_withDir, self).__init__()

        self.encoder_d = nn.Sequential(
                           ConvModule(in_channels//2+3, internal_depth//2, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth//2, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )
        self.encoder_s = nn.Sequential(
                           ConvModule(in_channels//2, internal_depth//2, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth//2, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )

        self.decoder = nn.Sequential(
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                           ConvModule(internal_depth, out_channels, ksize=kernel_size, pad=True, activation=last_activation, norm_layer=None, padding_mode="reflect")
                       )


    def forward(self, diffuse, specular, directions):
        diffuse_encoded = self.encoder_d(torch.cat((diffuse, directions), dim=1))
        specular_encoded = self.encoder_s(specular)
        x_out = self.decoder(torch.cat((diffuse_encoded, specular_encoded), dim=1))
        return x_out

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)

class FixUpResNet_withDir_DiffuseOnly(nn.Module):
    def __init__(self, in_channels, out_channels, internal_depth, blocks, kernel_size, dropout=0.0, last_activation=None):
        super(FixUpResNet_withDir_DiffuseOnly, self).__init__()

        self.encoder_d = nn.Sequential(
                           ConvModule(in_channels+3, internal_depth, ksize=kernel_size, pad=True, activation="relu", norm_layer=None, padding_mode="reflect"),
                           nn.Dropout(dropout),
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                       )

        self.decoder = nn.Sequential(
                           FixupResidualChain(internal_depth, ksize=kernel_size, depth=int(blocks/4), padding_mode="reflect", dropout=dropout),
                           ConvModule(internal_depth, out_channels, ksize=kernel_size, pad=True, activation=last_activation, norm_layer=None, padding_mode="reflect")
                       )


    def forward(self, diffuse, directions):
        diffuse_encoded = self.encoder_d(torch.cat((diffuse, directions), dim=1))
        x_out = self.decoder(diffuse_encoded)
        return x_out

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)