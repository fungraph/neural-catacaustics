import torch
from model_defs.warp_field_mlp import CatacausticMLP, ProgressiveCatacausticMLP
from utils.graphics_utils import projectToCamera, eigen, inverse, computeJacobian, geom_transform_points, percentile
import math
from diff_rasterization.rasterizer import PointsRasterizationSettings, PointsRasterizer
from scene_loaders.cameras import CameraSimple, PulsarCamera
from utils.general_utils import pbnrClamp
from utils.graphics_utils import projectEnvmap, getDirRays3
from utils.image_utils import crop_image
#from pytorch3d.renderer.points.pulsar import Renderer
from utils.graphics_utils import sensor_size, fov2focal
from utils.camera_utils import pulsarCamera_from_inputCamera, simpleCamera_from_inputCamera

def render_pc_pulsar(pulsar_camera, xyz, normals, features, alpha, uncertainty):
    features_sig = torch.sigmoid(features)
    renderer = Renderer(pulsar_camera.image_width,
                        pulsar_camera.image_height,
                        xyz.shape[0],
                        n_channels=features.shape[1],
                        right_handed_system=True).cuda()

    # Render.
    #start_iter_timer = torch.cuda.Event(enable_timing=True)
    #stop_iter_timer = torch.cuda.Event(enable_timing=True)
    #start_iter_timer.record()
    image = renderer(
        xyz,
        features_sig,
        torch.sqrt(uncertainty).squeeze(),
        pulsar_camera.cam_params,
        0.005,  # Renderer blending parameter gamma, in [1., 1e-5].
        200.0,  # Maximum depth.
    ).permute(2,0,1).unsqueeze(0)
    #stop_iter_timer.record()
    #torch.cuda.synchronize()
    #print(start_iter_timer.elapsed_time(stop_iter_timer))

    return image, None, None

def render_pc(viewpoint_camera, xyz, normals, features, alpha, uncertainty, uncertainty_scale=1.0):
    # start = torch.cuda.Event(enable_timing=True)
    # end = torch.cuda.Event(enable_timing=True)
    # start.record()

    # features_sig = torch.sigmoid(features[:, ])
    #features_sig = torch.cat((features[:, :3], torch.sigmoid(features[:, 3:])), dim=1)
    features_sig = torch.sigmoid(features)
    alpha = torch.sigmoid(alpha)

    ones = torch.ones((1, xyz.shape[0], 1), dtype=xyz.dtype, device="cuda")
    hom_point_cloud = torch.cat([xyz.unsqueeze(0), ones], dim=2)
    viewspace_points = projectToCamera(hom_point_cloud, viewpoint_camera)
    try:
        viewspace_points.retain_grad()
    except:
        pass
    #viewpoint_camera.viewspace_points = geom_transform_points(xyz, viewpoint_camera.full_proj_transform)

    filter_pos_x = torch.logical_and(viewspace_points[:, 0] < 1.0, viewspace_points[:, 0] > -1.0)
    filter_pos_y = torch.logical_and(viewspace_points[:, 1] < 1.0, viewspace_points[:, 1] > -1.0)
    filter_pos_z = viewspace_points[:, 2] > 0.2
    filter = torch.logical_and(torch.logical_and(filter_pos_x, filter_pos_y), filter_pos_z)

    filtered_pc = xyz[filter]
    filtered_uncertainty = uncertainty[filter]
    filtered_uncertainty *= uncertainty_scale

    if filtered_pc.shape[0] == 0:
        return (torch.zeros(1, features.shape[1], viewpoint_camera.image_height, viewpoint_camera.image_width).cuda(),
                torch.ones(1, 1, viewpoint_camera.image_height, viewpoint_camera.image_width).cuda(), None, None)

    if normals.nelement() == 0:
        filtered_normals = (filtered_pc - viewpoint_camera.camera_center) / (filtered_pc - viewpoint_camera.camera_center).norm(dim=1, keepdim=True)
    else:
        filtered_normals = normals[filter]
        filtered_normals = filtered_normals / filtered_normals.norm(2, dim=1, keepdim=True)

    h = viewpoint_camera.image_height /(2.0 * math.tan(viewpoint_camera.FoVy / 2.0))
    novel_view_jacobian = h*computeJacobian(filtered_pc, filtered_normals, viewpoint_camera)
    Vrk = torch.eye(2, device="cuda").repeat((novel_view_jacobian.shape[0],1,1))*filtered_uncertainty.unsqueeze(-1)
    #covariance = torch.bmm(novel_view_jacobian, novel_view_jacobian.transpose(1, 2)) #+ torch.eye(2).cuda()
    cov = novel_view_jacobian @ Vrk @ novel_view_jacobian.transpose(1, 2) + torch.eye(2).cuda() * 0.3

    #compute sigma without eigen?
    w, v = eigen(cov)
    pixel_sigma = int(torch.ceil(3 * torch.sqrt(w.max())))

    #cov = torch.matmul(v, torch.matmul(w.diag_embed(), v.transpose(-2, -1)))
    invertible_filter = cov.det() != 0.0
    inv_cov = inverse(cov[invertible_filter])

    filtered_alpha = alpha[filter][invertible_filter]
    filtered_alpha /= uncertainty_scale 

    raster_settings = PointsRasterizationSettings(
        image_height=int(viewpoint_camera.image_height),
        image_width=int(viewpoint_camera.image_width),
        znear=viewpoint_camera.znear,
        zfar=viewpoint_camera.zfar,
        gamma=1.0,
        layers=16,
        fastVersion=True
    )

    #rasterizer = PointsLayeredRasterizer(
    #   raster_settings=raster_settings
    #)
    rasterizer = PointsRasterizer(
       raster_settings=raster_settings
    )
    #rasterizer = PointsRasterizerCached(
    #    raster_settings=raster_settings
    #)

    #print(viewspace_points[filter].shape[0])

    col_image, mask, _, _ = rasterizer(viewspace_points[filter][invertible_filter],
                                    features_sig[filter][invertible_filter],
                                    filtered_alpha,
                                    normals,
                                    inv_cov,
                                    pixel_sigma)

    # end.record()
    # torch.cuda.synchronize()
    # print("Rasterizer {}", start.elapsed_time(end))

    return col_image, mask, filter, viewspace_points


def render_patch_pc(viewpoint_camera,
                    patch_size_y, patch_size_x, patch_origin_x, patch_origin_y,
                    xyz, normals, features, alpha, uncertainty, uncertainty_scale=1.0):

    pixel_size_y = (2 * math.tan(viewpoint_camera.FoVy / 2)) / viewpoint_camera.image_height
    pixel_size_x = (2 * math.tan(viewpoint_camera.FoVx / 2)) / viewpoint_camera.image_width
    cx = viewpoint_camera.image_width / 2
    cy = viewpoint_camera.image_height / 2
    top_pixel = cy - patch_origin_y
    bottom_pixel = top_pixel - patch_size_y
    right_pixel = cx - patch_origin_x
    left_pixel = right_pixel - patch_size_x

    camera_for_patch = CameraSimple(R=viewpoint_camera.R, T=viewpoint_camera.T,
                                    top=top_pixel * pixel_size_y, bottom=bottom_pixel * pixel_size_y,
                                    right=right_pixel * pixel_size_x, left=left_pixel * pixel_size_x,
                                    znear=viewpoint_camera.znear,
                                    zfar=viewpoint_camera.zfar,
                                    image_height=patch_size_y,
                                    image_width=patch_size_x,
                                    fovx=viewpoint_camera.FoVx,
                                    fovy=viewpoint_camera.FoVy,
                                    trans=viewpoint_camera.trans,
                                    scale=viewpoint_camera.scale)

    return render_pc(viewpoint_camera=camera_for_patch,
                     xyz=xyz, normals=normals, features=features,
                     alpha=alpha, uncertainty=uncertainty, uncertainty_scale=uncertainty_scale)


def render_point_cloud(viewpoint_camera, point_cloud, warp_field_mlp, patch, cata_camera=None, w_dampen=1.0, uncertainty_scale=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch
    xyz_to_render = point_cloud.global_xyz
    if warp_field_mlp is not None:
        # Warp the global PC
        if cata_camera is None:
            cata_camera = viewpoint_camera
        cam_center_expand = cata_camera.camera_center.expand(point_cloud.global_xyz.shape[0], 3)
        if isinstance(warp_field_mlp, ProgressiveCatacausticMLP):
            xyz_to_render = warp_field_mlp([xyz_to_render, cam_center_expand], w_dampen)
        else:
            xyz_to_render = warp_field_mlp([xyz_to_render, cam_center_expand])
       
    uncertainty = 0.2 * point_cloud.global_sigmoid_scale_u * torch.sigmoid(point_cloud.global_uncertainty)
    
    if point_cloud.active_normals:
        normals = point_cloud.global_normals
    else:
        normals = torch.tensor([], device="cuda")

    render, mask, filter, viewspace_points_tensor = render_patch_pc(viewpoint_camera=viewpoint_camera,
                           patch_origin_x=patch_origin_x, patch_size_x=patch_size_x,
                           patch_origin_y=patch_origin_y, patch_size_y=patch_size_y,
                           xyz=xyz_to_render,
                           normals=normals,
                           features=point_cloud.global_features,
                           alpha=point_cloud.global_alpha,
                           uncertainty=uncertainty,
                           uncertainty_scale=uncertainty_scale)

    return render, mask, filter, viewspace_points_tensor


def render_scene(viewpoint_camera, scene, iteration=None, patch=None, cata_camera=None, w_dampen=1.0, uncertainty_scale=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
        
    #start_diffuse = torch.cuda.Event(enable_timing=True)
    #end_diffuse = torch.cuda.Event(enable_timing=True)
    #start_specular = torch.cuda.Event(enable_timing=True)
    #end_specular = torch.cuda.Event(enable_timing=True)

    #start_diffuse.record()
    diffuse_render, diffuse_environment_mask, diffuse_filter, viewspace_points_tensor = render_point_cloud(viewpoint_camera, scene.diffuse_point_cloud, None, patch)

    bg_render=None
    if scene.bg_type == "env_map":
        bg_render = torch.sigmoid(projectEnvmap(viewpoint_camera, scene.environment_map, patch))
    elif scene.bg_type == "point_cloud":
        bg_render, _, _ = render_point_cloud(viewpoint_camera, scene.background_point_cloud, None, patch)
    diffuse_render_with_envmap = diffuse_environment_mask*bg_render + diffuse_render[:,:-1,...]
    #end_diffuse.record()

    #start_specular.record()
    specular_render, specular_environment_mask, filter, _ = render_point_cloud(viewpoint_camera,
                                                                    scene.specular_point_cloud,
                                                                    scene.warp_field_mlp,
                                                                    patch, cata_camera, 
                                                                    w_dampen, uncertainty_scale)
    #end_specular.record()
    dirs = crop_image(-getDirRays3(viewpoint_camera, sigma=0.01), patch)
    if scene.use_mask:
        mask = diffuse_render[:, -1:, ...]
        if iteration:
            blend = min(iteration / 20000, 1.0)
        else:
            blend = 1.0
        d_mask = blend * (1.0 - mask) + (1.0 - blend) * 1.0
        s_mask = blend * (mask) + (1.0 - blend) * 1.0
        view = scene.view_renderer(d_mask*diffuse_render_with_envmap, s_mask*specular_render, d_mask*dirs)
    else:
        view = scene.view_renderer(diffuse_render_with_envmap, specular_render, dirs)
        mask = torch.ones_like(view)
    #torch.cuda.synchronize()
    #print("{}x{} Diffuse {} Specular {}".format(viewpoint_camera.image_height, viewpoint_camera.image_width,
    #      start_diffuse.elapsed_time(end_diffuse), start_specular.elapsed_time(end_specular)))

    return view, diffuse_render_with_envmap, specular_render,\
           mask, specular_environment_mask, filter, viewspace_points_tensor, diffuse_filter

def render_scene_from_OGL(viewpoint_camera, scene, diffuse_render, diffuse_environment_mask, iteration=None, patch=None, cata_camera=None, uncertainty_scale=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    
    env_map = projectEnvmap(viewpoint_camera, scene.environment_map, patch)
    diffuse_render_with_envmap = diffuse_environment_mask*torch.sigmoid(env_map) + diffuse_render[:,:-1,...]
    
    specular_render, specular_environment_mask, _, _ = render_point_cloud(viewpoint_camera, scene.specular_point_cloud, scene.warp_field_mlp, patch, cata_camera, uncertainty_scale=uncertainty_scale)

    dirs = crop_image(-getDirRays3(viewpoint_camera, sigma=0.01), patch)
    if scene.use_mask:
        mask = diffuse_render[:, -1:, ...]
        if iteration:
            blend = min(iteration / 20000, 1.0)
        else:
            blend = 1.0
        d_mask = blend * (1.0 - mask) + (1.0 - blend) * 1.0
        s_mask = blend * (mask) + (1.0 - blend) * 1.0
        view = scene.view_renderer(d_mask*diffuse_render_with_envmap, s_mask*specular_render, d_mask*dirs)
    else:
        view = scene.view_renderer(diffuse_render_with_envmap, specular_render, dirs)
        mask = torch.ones_like(view)
    return view, diffuse_render_with_envmap, specular_render, mask, specular_environment_mask


def render_scene_diffuse_only(viewpoint_camera, scene, patch=None, use_renderer=True):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)

    diffuse_pc_rast, environment_mask, diffuse_filter, viewspace_points_tensor = render_point_cloud(viewpoint_camera, scene.diffuse_point_cloud, None, patch)

    env_map = projectEnvmap(viewpoint_camera, scene.environment_map, patch)

    diffuse_render_with_envmap = environment_mask*torch.sigmoid(env_map) + diffuse_pc_rast[:,:-1,...]

    dirs = crop_image(-getDirRays3(viewpoint_camera, sigma=0.01), patch)

    view = None
    if use_renderer:
        view = scene.view_renderer(diffuse_render_with_envmap, dirs)

    return view, diffuse_render_with_envmap, environment_mask, viewspace_points_tensor, diffuse_filter


def render_scene_specular_only(viewpoint_camera, scene, patch=None, cata_camera=None, uncertainty_scale=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)

    specular_render, _, _, _ = render_point_cloud(viewpoint_camera,
                                                scene.specular_point_cloud,
                                                scene.warp_field_mlp,
                                                patch, cata_camera, uncertainty_scale=uncertainty_scale)
    return specular_render


def warp_point_cloud(point_cloud, warp_field_mlp, viewpoint_camera, cata_camera=None):
    xyz_to_render = point_cloud
    if cata_camera is None:
        cata_camera = viewpoint_camera
    cam_center_expand = cata_camera.camera_center.expand(point_cloud.shape[0], 3)
    return warp_field_mlp([xyz_to_render, cam_center_expand])