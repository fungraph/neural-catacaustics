from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL.shaders import compileProgram, compileShader
import numpy as np
import copy
import math

#=======================================================

def getUtilsDir():
    return os.path.dirname(os.path.realpath(__file__))

def upDir(x):
   return os.path.dirname(x)

sys.path.append(getUtilsDir())
sys.path.append(upDir(upDir(getUtilsDir())))
#from dnnlib.util import EasyDict

#=======================================================

class Rendertarget:

    def __init__(self):
        self.color = None
        self.depth = None

#=======================================================

# initialize OpenGL context
def oglInit(res=5, title="OpenGL Window"):
    if isinstance(res, int):
        res = (res, res)
    glutInit()
    glutInitWindowSize(res[0], res[1])
    window = glutCreateWindow(title)
    return window
    
#=======================================================

# create a framebuffer
def createFramebuffer(n=1):
    return glGenFramebuffers(n)

#=======================================================

# delete a framebuffer
def deleteFramebuffer(fbo):
    glDeleteFramebuffers(1, [fbo])

#=======================================================

# create a shader program, consisting of vertex and fragment shaders
def createShader(pathVertex, pathFragment):
    vertexSrc = open(pathVertex + ".vert", "r").read()
    fragmentSrc = open(pathFragment + ".frag", "r").read()
    shader = compileProgram(compileShader(vertexSrc, GL_VERTEX_SHADER), compileShader(fragmentSrc, GL_FRAGMENT_SHADER))
    return shader

#=======================================================

# create a texture
def createTexture(n=1):
    return glGenTextures(n)

#=======================================================

# delete a texture
def deleteTexture(t):
     glDeleteTextures(1, [t])

#=======================================================

# create render targets for rendering to a texture
def createRenderTargets(res, createDepthBuffer, channelCount=4):

    if isinstance(res, int):
        res = (res, res)
    
    rts = Rendertarget()
    rts.color = createTexture()
    glBindTexture(GL_TEXTURE_2D, rts.color)
    glFormat = glFormatFromChannelCount(channelCount)
    glTexImage2D(GL_TEXTURE_2D, 0, glFormat[0], res[0], res[1], 0, glFormat[1], GL_FLOAT, None)
    if createDepthBuffer:
        rts.depth = createTexture()
        glBindTexture(GL_TEXTURE_2D, rts.depth)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, res[0], res[1], 0, GL_DEPTH_COMPONENT, GL_FLOAT, None)
    return rts

#=======================================================

# delete render targets
def deleteRenderTargets(rts):
    if rts.color is not None:
        deleteTexture(rts.color)
    if rts.depth is not None:
        deleteTexture(rts.depth)

#=======================================================

# build a linear MIP map
def buildMIP(tex):
    glBindTexture(GL_TEXTURE_2D, tex)
    glGenerateMipmap(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, 0)

#=======================================================

# set texture sampling parameters
def setTexParams(target=GL_TEXTURE_2D, minFilter=GL_NEAREST, magFilter=GL_NEAREST, wrap=GL_CLAMP_TO_BORDER):
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter)
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter)
    glTexParameterfv(target, GL_TEXTURE_BORDER_COLOR, [0, 0, 0, 1])
    glTexParameteri(target, GL_TEXTURE_WRAP_S, wrap)
    glTexParameteri(target, GL_TEXTURE_WRAP_T, wrap)

#=======================================================

# get default OpenGL (internal) format from channel count
def glFormatFromChannelCount(c):
    if c == 1:
        return GL_R32F, GL_RED
    if c == 2:
        return GL_RG32F, GL_RG
    if c == 3:
        return GL_RGB32F, GL_RGB
    if c == 4:
        return GL_RGBA32F, GL_RGBA

#=======================================================

# upload an image to the GPU
def uploadImage(image, texture=None, flipH=True):
    if image.ndim == 2:
        image = image[..., None]
    w, h, c = image.shape
    if flipH:
        image = np.flip(image, 0)
    if texture is None:
        texture = createTexture()
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glBindTexture(GL_TEXTURE_2D, texture)
    glFormat = glFormatFromChannelCount(c)
    glTexImage2D(GL_TEXTURE_2D, 0, glFormat[0], h, w, 0, glFormat[1], GL_FLOAT, image)
    glBindTexture(GL_TEXTURE_2D, 0)
    return texture

#=======================================================

# upload a list of images to a texture array
def uploadImageList(imgList, textureArray=None, flipH=True):
    imgCount = len(imgList)
    assert imgCount > 0, "List is empty."
    w, h, c = imgList[0].shape
    _imgList = copy.deepcopy(imgList)
    for i in range(imgCount):
        assert imgList[0].shape == imgList[i].shape, "Image dimensions don't match."
        if flipH:
            _imgList[i] = np.flip(_imgList[i], 0)
    if textureArray is None:
        textureArray = createTexture()
    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray)
    glinternalformat = GL_RGBA32F if c == 4 else GL_RGB32F
    glformat = GL_RGBA if c == 4 else GL_RGB
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, glinternalformat, h, w, imgCount)
    for i in range(imgCount):
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, h, w, 1, glformat, GL_FLOAT, _imgList[i])
    glBindTexture(GL_TEXTURE_3D, 0)
    return textureArray

#=======================================================

# download an image from the GPU
def downloadImage(texture, flipH=True, channels=4):
    glBindTexture(GL_TEXTURE_2D, texture)

    # PyOpenGL does not support downloading GL_RG textures
    # therefore: make it GL_RGB temporarily
    tempChannels = 3 if channels == 2 else channels
    
    _, glFormat = glFormatFromChannelCount(tempChannels)
    image = glGetTexImage(GL_TEXTURE_2D, 0, glFormat, GL_FLOAT)
    glBindTexture(GL_TEXTURE_2D, 0)
    if channels == 1:
        image = image[..., None]
    w, h, c = image.shape
    image = np.reshape(image, (h, w, c))

    # if temp channel was necessary, throw it away
    if not tempChannels == channels:
        image = image[..., 0:2]

    if flipH:
        image = np.flip(image, 0)
    return image

#=======================================================

# render a screen-filling quad
def renderScreenQuad():
	glBegin(GL_QUADS)
	glTexCoord2f(0, 0)
	glVertex2f(-1, -1)
	glTexCoord2f(0, 1)
	glVertex2f(1, -1)
	glTexCoord2f(1, 1)
	glVertex2f(1, 1)
	glTexCoord2f(1, 0)
	glVertex2f(-1, 1)
	glEnd()

#=======================================================

# render quads from a vertex list
def renderQuads(vertices):
    glBegin(GL_QUADS)
    for v in vertices:
        glVertex3f(*v)
    glEnd()

#=======================================================

class OpenGLPointCloud:

    def __init__(self, points, data_path=None):

        def sigmoid(x):
            return 1.0 / (1 + np.exp(-x))

        eps = 0.000001

        self.num_points, num_features = points.global_features.shape

        data = None

        # position, normal, alpha, uncertainty, features
        num_entries = 3 + 3 + 1 + 1 + num_features

        if data_path is None or not os.path.isfile(data_path):

            if data_path is not None:
                print("\tCaching ogl_data to disk. This only happens the first time the viewer is run for this scene.")
            else:
                print("\tConsider providing a data_path so that data can be cached.")

            positions = points.global_xyz.detach().cpu().numpy()
            normals = points.global_normals.detach().cpu().numpy()
            alphas = points.global_alpha.detach().cpu().numpy()
            uncertainties = points.global_uncertainty.detach().cpu().numpy()
            sigmoid_scales = points.global_sigmoid_scale_u.detach().cpu().numpy()
            features = points.global_features.detach().cpu().numpy()

            data = np.concatenate((positions, normals, sigmoid(alphas),
                                  0.2*sigmoid(uncertainties)*sigmoid_scales,
                                  sigmoid(features)), axis=1)

            data = data.flatten()

            if data_path is not None and not os.path.isfile(data_path):
                np.save(data_path, data)

        elif data_path is not None:
            print("\tLoading cached data from disk...")
            data = np.load(data_path)


        #---------------------------------
        # upload data to the GPU

        self.vao = glGenVertexArrays(1)
        self.point_buffer = glGenBuffers(1)

        glBindVertexArray(self.vao)
        glBindBuffer(GL_ARRAY_BUFFER, self.point_buffer)
        glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW)
    
        elem_size = data[0].nbytes
        stride = elem_size * num_entries

        # position
        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p(0))
        
        # normal
        glEnableVertexAttribArray(1)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p(3 * elem_size))
        
        # alpha + uncertainties
        glEnableVertexAttribArray(2)
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p(6 * elem_size))

        # features
        num_feature_inputs = int(math.ceil(num_features / 4.))
        for f_idx in range(num_feature_inputs):
            glEnableVertexAttribArray(3+f_idx)
            glVertexAttribPointer(3+f_idx, 4, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p((8+4*f_idx) * elem_size))
            
        glBindVertexArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)



#=======================================================

class OpenGLMesh:

    def __init__(self, vertices, tris, computeNormals=True):
        
        self.vertices = vertices
        self.tris = tris
        self.triIndexCount = self.tris.size

        num_entries = 3

        if computeNormals:
            self.createVertexNormals()
            num_entries += 3
            bufferContent = np.transpose(np.stack((self.vertices, self.normals)), (1, 0, 2))
        else:
            bufferContent = self.vertices

        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)
        
        self.bufs = glGenBuffers(2)
        glBindBuffer(GL_ARRAY_BUFFER, self.bufs[0])
        glBufferData(GL_ARRAY_BUFFER, bufferContent.flatten(), GL_STATIC_DRAW)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.bufs[1])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, self.tris.flatten(), GL_STATIC_DRAW)
        
        elem_size = self.vertices[0,0].nbytes
        stride = elem_size * num_entries

        glEnableVertexAttribArray(0)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p(0))
        
        if computeNormals:
            glEnableVertexAttribArray(1)
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, ctypes.c_void_p(3 * elem_size))

        glBindVertexArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

    #---------------------------------

    def __del__(self):
        glDeleteBuffers(2, self.bufs)
        glDeleteVertexArrays(1, [self.vao])

    #---------------------------------

    def createVertexNormals(self):
        
        # no normalization here => larger tris get a higher weight
        def getTriNormal(tri):
            p1, p2, p3 = self.vertices[tri]
            return np.cross(p1 - p2, p1 - p3)
    
        self.normals = np.zeros_like(self.vertices)

        for idx, v in enumerate(self.vertices):
            adjacentTris = np.argwhere(self.tris == idx)[:, 0]
            for at in adjacentTris:
                self.normals[idx] += getTriNormal(self.tris[at])
            self.normals[idx] /= np.linalg.norm(self.normals[idx])
