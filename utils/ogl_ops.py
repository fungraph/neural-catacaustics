import os, sys

def getUtilsDir():
    return os.path.dirname(os.path.realpath(__file__))

sys.path.append(getUtilsDir())

from ogl_utils import *

#=======================================================

# base class for OpenGL operations
class OpenGLOP:

    def __init__(
        self,
        #fbo,
        vertexShaders, fragmentShaders, 
        renderTargetCount, 
        renderTargetResolution=None,
        createDepthRenderTargets=False):
        
        #self.fbo = fbo

        # init shader(s)
        shaderDir = getUtilsDir() + "/shaders/"
        self.shaders = []
        if not isinstance(vertexShaders, list):
            vertexShaders = [vertexShaders]
        if not isinstance(fragmentShaders, list):
            fragmentShaders = [fragmentShaders]
        if len(vertexShaders) == 1 and len(fragmentShaders) > 1:
            vertexShaders = len(fragmentShaders) * vertexShaders
        for v, f in zip(vertexShaders, fragmentShaders):
            self.shaders.append(createShader(shaderDir + v, shaderDir + f))
        if len(self.shaders) == 1:
            self.shader = self.shaders[0]

        # init uniforms
        self.uniforms = {}
        
        # init render target(s)
        self.renderTargetCount = renderTargetCount
        if renderTargetCount > 0:
            assert renderTargetResolution is not None, "Need resolution to create render target."
            self.rtRes = renderTargetResolution
            if isinstance(self.rtRes, int):
                self.rtRes = (self.rtRes, self.rtRes)
            self.rendertargets = []
            for _ in range(renderTargetCount):
                self.rendertargets.append(createRenderTargets(self.rtRes, createDepthRenderTargets))

    def getUniform(self, name, shaderIdx=0):
        return glGetUniformLocation(self.shaders[shaderIdx], name)

#=======================================================

# Render a textured screen quad.
class TexturedScreenQuadOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "vertex2Duv", "textured_quad", 1, resolution)
        
    def render(self, tex, toScreen):
        
        if not toScreen:
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)

        glDisable(GL_CULL_FACE)
        glDisable(GL_DEPTH_TEST)
        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glUseProgram(self.shader)

        glActiveTexture(GL_TEXTURE0)
    
        glBindTexture(GL_TEXTURE_2D, tex)
        setTexParams(GL_TEXTURE_2D)

        minFilter = GL_NEAREST
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter)
        
        glClear(GL_COLOR_BUFFER_BIT)
        renderScreenQuad()
        glBindTexture(GL_TEXTURE_2D, 0)
        glUseProgram(0)

        if not toScreen:
            return self.rendertargets[0].color
        else:
            return None
    

#=======================================================

# Visibility splatting for EWA rendering
# http://graphics.cs.cmu.edu/projects/objewa/eg2002print.pdf (Sec. 5.1)
class VisibilitySplattingOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "visibility_splat", "visibility_splat", 2, resolution, True)
        self.uniforms["viewMatrix"] = self.getUniform("viewMatrix")
        self.uniforms["projectionMatrix"] = self.getUniform("projectionMatrix")
        self.uniforms["h"] = self.getUniform("h")
        self.uniforms["usePrevDepth"] = self.getUniform("usePrevDepth")

    #------------------------------------------
    
    def render(self, points, cam, layer=0, prevDepth=None):
            
        glBindVertexArray(points.vao)
        glUseProgram(self.shader)
    
        glEnable(GL_PROGRAM_POINT_SIZE)
        glEnable(GL_POINT_SPRITE)

        glEnable(GL_DEPTH_TEST)
        
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[layer].color, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, self.rendertargets[layer].depth, 0)

        viewMatrix = cam.world_view_transformCPU[0]
        projectionMatrix = cam.projection_matrixCPU[0].numpy()
        glProgramUniformMatrix4fv(self.shader, self.uniforms["viewMatrix"], 1, True, viewMatrix)
        glProgramUniformMatrix4fv(self.shader, self.uniforms["projectionMatrix"], 1, True, projectionMatrix)

        h = self.rtRes[1] / (2.0 * math.tan(cam.FoVy / 2.0))
        glProgramUniform1f(self.shader, self.uniforms["h"], h)
        
        if prevDepth is None:
            glProgramUniform1i(self.shader, self.uniforms["usePrevDepth"], 0)
        else:
            glProgramUniform1i(self.shader, self.uniforms["usePrevDepth"], 1)
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, prevDepth)
            setTexParams(GL_TEXTURE_2D)

        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glDrawArrays(GL_POINTS, 0, points.num_points)

        glUseProgram(0)
        glDisable(GL_PROGRAM_POINT_SIZE)
        glDisable(GL_POINT_SPRITE)
        glDisable(GL_DEPTH_TEST)
        glBindTexture(GL_TEXTURE_2D, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0)
        glBindVertexArray(0)
    
        return self.rendertargets[layer].color
        
#=======================================================

# Rasterize a point cloud with EWA splats
class PointRasterizationOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "point_rast", "point_rast", 7, resolution, False)
        self.uniforms["viewMatrix"] = self.getUniform("viewMatrix")
        self.uniforms["projectionMatrix"] = self.getUniform("projectionMatrix")
        self.uniforms["h"] = self.getUniform("h")

    #------------------------------------------
    
    def render(self, points, cam, depth1, depth2):
            
        glBindVertexArray(points.vao)
        glUseProgram(self.shader)
    
        glEnable(GL_PROGRAM_POINT_SIZE)
        glEnable(GL_POINT_SPRITE)

        glEnable(GL_BLEND)
        glBlendEquation(GL_FUNC_ADD)
        glBlendFunc(GL_ONE, GL_ONE)

        glDrawBuffers(self.renderTargetCount, [GL_COLOR_ATTACHMENT0 + i for i in range(self.renderTargetCount)])
        for i in range(self.renderTargetCount):
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, self.rendertargets[i].color, 0)
    
        viewMatrix = cam.world_view_transformCPU[0]
        projectionMatrix = cam.projection_matrixCPU[0].numpy()
        glProgramUniformMatrix4fv(self.shader, self.uniforms["viewMatrix"], 1, True, viewMatrix)
        glProgramUniformMatrix4fv(self.shader, self.uniforms["projectionMatrix"], 1, True, projectionMatrix)

        h = self.rtRes[1] / (2.0 * math.tan(cam.FoVy / 2.0))
        glProgramUniform1f(self.shader, self.uniforms["h"], h)
        
        glActiveTexture(GL_TEXTURE0)    
        glBindTexture(GL_TEXTURE_2D, depth1)
        setTexParams(GL_TEXTURE_2D)

        glActiveTexture(GL_TEXTURE1)    
        glBindTexture(GL_TEXTURE_2D, depth2)
        setTexParams(GL_TEXTURE_2D)

        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glDrawArrays(GL_POINTS, 0, points.num_points)

        glUseProgram(0)
        glActiveTexture(GL_TEXTURE0)    
        glDisable(GL_PROGRAM_POINT_SIZE)
        glDisable(GL_POINT_SPRITE)
        glDisable(GL_BLEND)
        glBindTexture(GL_TEXTURE_2D, 0)
        glBindVertexArray(0)

        return [self.rendertargets[i].color for i in range(self.renderTargetCount)]        

#=======================================================

# layered EWA compositing
class CompositingOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "vertex2Duv", "compositing", 2, resolution)
        
    def render(self, textures):
        
        glDrawBuffers(self.renderTargetCount, [GL_COLOR_ATTACHMENT0 + i for i in range(self.renderTargetCount)])
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, self.rendertargets[1].color, 0)

        glDisable(GL_CULL_FACE)
        glDisable(GL_DEPTH_TEST)
        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glUseProgram(self.shader)

        for idx, t in enumerate(textures):
            glActiveTexture(GL_TEXTURE0+idx)    
            glBindTexture(GL_TEXTURE_2D, t)
            setTexParams(GL_TEXTURE_2D)

        glClear(GL_COLOR_BUFFER_BIT)
        renderScreenQuad()
        
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, 0)
        glUseProgram(0)

        return self.rendertargets[0].color, self.rendertargets[1].color
    

    #=======================================================

# Render a textured screen quad with visualization overlays
class VisualizationOverlayOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "vertex2Duv", "visualization_overlay", 1, resolution)
        self.uniforms["pointPosition"] = self.getUniform("pointPosition")

        
    def render(self, backgroundTex, vizTex1, vizTex2, pointPosition):
        
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)

        glDisable(GL_CULL_FACE)
        glDisable(GL_DEPTH_TEST)
        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glUseProgram(self.shader)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, backgroundTex)
        setTexParams(GL_TEXTURE_2D)

        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, vizTex1 if vizTex1 is not None else 0)
        setTexParams(GL_TEXTURE_2D)
        
        glActiveTexture(GL_TEXTURE2)
        glBindTexture(GL_TEXTURE_2D, vizTex2 if vizTex2 is not None else 0)
        setTexParams(GL_TEXTURE_2D)
           
        glProgramUniform2f(self.shader, self.uniforms["pointPosition"], *pointPosition)

        minFilter = GL_NEAREST
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter)
        
        glClear(GL_COLOR_BUFFER_BIT)
        renderScreenQuad()
        glBindTexture(GL_TEXTURE_2D, 0)
        glUseProgram(0)

        return self.rendertargets[0].color




#=======================================================

class PointCloud2DRenderOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "vertex2D", "simple_point_rast", 1, resolution)
        self.uniforms["scale"] = self.getUniform("scale")
        

    def render(self, points2D, pointSize=3, scale=0.9):
    
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)

        glDisable(GL_CULL_FACE)
        glDisable(GL_DEPTH_TEST)
        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        glUseProgram(self.shader)
        
        glProgramUniform1f(self.shader, self.uniforms["scale"], scale)
        
        glClear(GL_COLOR_BUFFER_BIT)

        glPointSize(pointSize * 3)
        glBegin(GL_POINTS)
        glColor4f(1, 1, 0, 1)
        glVertex2f(*points2D[0])
        glEnd()

        glPointSize(pointSize)
        glBegin(GL_POINTS)
        glColor4f(0, 0.5, 0.4, 1)
        for p in points2D[1:]:
            glVertex2f(*p)
        glEnd()
        
        glUseProgram(0)
        return self.rendertargets[0].color
    

#=======================================================

class MeshRenderOP(OpenGLOP):

    def __init__(self, resolution, fov=0.35):
        OpenGLOP.__init__(self, "mesh_rast", "mesh_rast", 1, resolution, True)
        self.uniforms["modelViewMatrix"] = self.getUniform("modelViewMatrix")    
        self.uniforms["projectionMatrix"] = self.getUniform("projectionMatrix")    
        self.uniforms["lightDirection"] = self.getUniform("lightDirection")
        self.uniforms["factor"] = self.getUniform("factor")
        self.uniforms["cameraPoint"] = self.getUniform("cameraPoint")
        self.projectionMatrix = self.createProjectionMatrix(fov)


    def createProjectionMatrix(self, fovy, aspect=1, znear=0.1, zfar=10):
        scale = 1. / math.tan(0.5 * fovy)
        proj = np.zeros((4,4))
        proj[0, 0] = scale / aspect
        proj[1, 1] = scale
        proj[3, 2] = -1
        proj[2, 2] = - (zfar + znear) / (zfar - znear)
        proj[2, 3] = - 2 * zfar * znear / (zfar - znear)
        return proj

    def render(self, mesh, modelViewMatrix, cameraPoint=None, lightDirection=[0, 0, 1], wireframeFactor=1.2):
        
        if cameraPoint is not None:
            cameraPoint = self.projectionMatrix @ modelViewMatrix @ np.array([*cameraPoint, 1])
            cameraPoint = cameraPoint[0:2] / cameraPoint[3]
            cameraPoint = (0.5 * cameraPoint + 0.5) * self.rtRes
        else:
            cameraPoint = -100 * np.ones(2)

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, self.rendertargets[0].depth, 0)
        
        glActiveTexture(GL_TEXTURE0)
        setTexParams(GL_TEXTURE_2D)

        glUseProgram(self.shader)
        glEnable(GL_DEPTH_TEST)
        glViewport(0, 0, self.rtRes[0], self.rtRes[1])
        
        glBindVertexArray(mesh.vao)
  
        glProgramUniformMatrix4fv(self.shader, self.uniforms["modelViewMatrix"], 1, True, modelViewMatrix)
        glProgramUniformMatrix4fv(self.shader, self.uniforms["projectionMatrix"], 1, True, self.projectionMatrix)
        glProgramUniform3f(self.shader, self.uniforms["lightDirection"], *lightDirection)
        glProgramUniform1f(self.shader, self.uniforms["factor"], 1.)
        glProgramUniform2f(self.shader, self.uniforms["cameraPoint"], *cameraPoint)
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glDrawElements(GL_TRIANGLES, mesh.triIndexCount, GL_UNSIGNED_INT, None)  
        
        if wireframeFactor != 1:
            glClear(GL_DEPTH_BUFFER_BIT)
            glProgramUniform1f(self.shader, self.uniforms["factor"], wireframeFactor)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
            glDrawElements(GL_TRIANGLES, mesh.triIndexCount, GL_UNSIGNED_INT, None)  
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        glUseProgram(0)
        glDisable(GL_DEPTH_TEST)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0)
        glBindTexture(GL_TEXTURE_2D, 0)
        glBindVertexArray(0)

        return self.rendertargets[0].color


#=======================================================

# Two textures to one anaglyph texture
class AnaglyphOP(OpenGLOP):

    def __init__(self, resolution):
        OpenGLOP.__init__(self, "vertex2Duv", "anaglyph", 1, resolution, False)


    def render(self, leftTexture, rightTexture):
         
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.rendertargets[0].color, 0)

        glViewport(0, 0, self.rtRes[0], self.rtRes[1])

        glDisable(GL_CULL_FACE)
        glDisable(GL_DEPTH_TEST)

        glUseProgram(self.shader)
        
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, leftTexture)
        setTexParams()

        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, rightTexture)
        setTexParams()
        
        glClear(GL_COLOR_BUFFER_BIT)
        renderScreenQuad()

        return self.rendertargets[0].color
        
