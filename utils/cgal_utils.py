from sys import platform
import os

if platform == 'win32':
    cgal_executable_path  = os.path.join(os.path.dirname(__file__),
                                         "../extern/CGAL-5.4/examples/Point_set_processing_3/build/RelWithDebInfo/edge_aware_upsample_point_set_example.exe")
elif platform == "linux" or platform == "linux2":
    cgal_executable_path = os.path.join(os.path.dirname(__file__),
                                        "../extern/CGAL-5.4/examples/Point_set_processing_3/edge_aware_upsample_point_set_example")
