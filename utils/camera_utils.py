import math
from scene_loaders.cameras import CameraSimple, PulsarCamera, Camera
import torch
#from pytorch3d.transforms.rotation_conversions import matrix_to_axis_angle
from utils.graphics_utils import sensor_size, fov2focal
import sys
import numpy as np
import os
from utils.general_utils import PILtoTorch
from PIL import Image
from scene_loaders.read_scenes_types import readBunderFolder
from scene_loaders.cameras import getSimpleCamera
from os import makedirs
from scene_loaders.colmap_loader import rotmat2qvec

def get_test_cameras(args, resolution_scale=1.0):

    print("Reading test cameras...")

    test_camera_folder = os.path.join(args.input_path, "test_path_cameras")
    test_infos = readBunderFolder(test_camera_folder, name_ints=5)

    cams = cameraList_from_camInfos(test_infos,
                                    resolution_scale, args,
                                    polytope_folder="test_polytope_masks")
    return cams

def cameraList_from_camInfos(cam_infos, resolution_scale, args, polytope_folder=None, full_cam=True):
    camera_list = []
    for idx, cam_info in enumerate(cam_infos):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("{}/{}".format(idx, len(cam_infos)))
        sys.stdout.flush()

        if isinstance(args.global_downscale, np.ndarray):
            resolution = args.global_downscale
        else:  # should be a type that converts to float
            orig_w, orig_h = cam_info.image.size
            global_down = orig_w / args.global_downscale
            scale = float(global_down) * float(resolution_scale)
            resolution = (int(orig_w / scale), int(orig_h / scale))
        if full_cam:
            resized_image_rgb = PILtoTorch(cam_info.image, resolution)

            polytope_mask = None
            if polytope_folder:
                polytope_mask_path = os.path.join(args.input_path, polytope_folder, cam_info.image_name + ".png")
                if os.path.isfile(polytope_mask_path):
                    img = Image.open(polytope_mask_path).convert('L')
                    polytope_mask = PILtoTorch(img, resolution)

            gt_image = resized_image_rgb[:, :3, ...]
            loaded_mask = None
            if resized_image_rgb.shape[1] == 4:
                loaded_mask = resized_image_rgb[:, 3:4, ...]

            camera_list.append(Camera(colmap_id=cam_info.uid, R=cam_info.R,
                                      T=cam_info.T, FoVx=cam_info.FovX,
                                      FoVy=cam_info.FovY, image=gt_image,
                                      gt_alpha_mask=loaded_mask,
                                      polytope_mask=polytope_mask,
                                      image_name=cam_info.image_name,
                                      max_radius=args.max_radius,
                                      uid=len(camera_list)))
        else:
            camera_list.append(getSimpleCamera(cam_info.R, cam_info.T, cam_info.FovY,
                                               cam_info.FovX, resolution[1], resolution[0],
                                               0.1, 100.0))
    print()
    return camera_list

def simpleCamera_from_inputCamera(input_camera, patch=None):
    if patch is None:
        patch = (0, 0, input_camera.image_width, input_camera.image_height)

    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch

    pixel_size_y = (2 * math.tan(input_camera.FoVy / 2)) / input_camera.image_height
    pixel_size_x = (2 * math.tan(input_camera.FoVx / 2)) / input_camera.image_width
    cx = input_camera.image_width / 2
    cy = input_camera.image_height / 2
    top_pixel = cy - patch_origin_y
    bottom_pixel = top_pixel - patch_size_y
    right_pixel = cx - patch_origin_x
    left_pixel = right_pixel - patch_size_x

    return CameraSimple(R=input_camera.R, T=input_camera.T,
                        top=top_pixel * pixel_size_y, bottom=bottom_pixel * pixel_size_y,
                        right=right_pixel * pixel_size_x, left=left_pixel * pixel_size_x,
                        znear=input_camera.znear,
                        zfar=input_camera.zfar,
                        image_height=patch_size_y,
                        image_width=patch_size_x,
                        fovx=input_camera.FoVx,
                        fovy=input_camera.FoVy,
                        trans=input_camera.trans,
                        scale=input_camera.scale)


def pulsarCamera_from_inputCamera(input_camera, patch):
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch

    r_mat = torch.tensor(input_camera.R)
    cam_params = torch.cat((input_camera.camera_center.cpu().squeeze(),
                            matrix_to_axis_angle(torch.tensor(r_mat)),
                            torch.tensor([fov2focal(input_camera.FoVx, 1.0)]),
                            torch.tensor([sensor_size(input_camera.FoVx, patch_size_x / input_camera.image_width)]),
                            torch.tensor([(patch_origin_x + patch_size_x / 2.0) - input_camera.image_width / 2]),
                            torch.tensor([(patch_origin_y + patch_size_y / 2.0) - input_camera.image_height / 2]),
                            )).float().cuda()
    return PulsarCamera(cam_params, image_width=patch_size_x, image_height=patch_size_y)

def exportListCamsToColmap(cam_list, outpath):
    makedirs(os.path.join(outpath, "stereo", "sparse"), exist_ok=True)
    images_file_path = os.path.join(outpath, "stereo", "sparse", "images.txt")
    cameras_file_path = os.path.join(outpath, "stereo", "sparse", "cameras.txt")

    with open(images_file_path, 'w') as i_f, open(cameras_file_path, 'w') as c_f:
        i_f.write("""# Image list with two lines of data per image:
#   IMAGE_ID, QW, QX, QY, QZ, TX, TY, TZ, CAMERA_ID, NAME
#   POINTS2D[] as (X, Y, POINT3D_ID)
""")
        c_f.write("""# Camera list with one line of data per camera:
#   CAMERA_ID, MODEL, WIDTH, HEIGHT, PARAMS[]
# Number of cameras: {} 
""".format(len(cam_list)))
        for idx, cam in enumerate(cam_list):
            id = idx+1
            q_vec = rotmat2qvec(cam.R)
            t_vec = cam.T
            i_f.write(f"{id} {-q_vec[0]} {q_vec[1]} {q_vec[2]} {q_vec[3]} {t_vec[0]} {t_vec[1]} {t_vec[2]} {id} {cam.image_name}\n")
            i_f.write("\n")
            w = cam.image_width
            h = cam.image_height
            fx = fov2focal(cam.FoVx, cam.image_width)
            fy = fov2focal(cam.FoVy, cam.image_height)
            c_f.write(f"{id} PINHOLE {w} {h} {fx} {fy} {w/2.0} {h/2.0}\n")
