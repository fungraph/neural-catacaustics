#version 420

layout(location=0) out vec4 out_color;

uniform vec3 lightDirection;
uniform float factor;
uniform vec2 cameraPoint;

in vec3 vNormal;

void main()
{	
	float offset = 0.1;
	float shading = (1 - offset) * abs(dot(lightDirection, vNormal)) + offset;
	shading = 0.8 * factor * shading;
	out_color = vec4(vec3(shading), 1);

	// camera point overlay
	ivec2 pixCoord = ivec2(gl_FragCoord.xy);
	int radius = 7;
	float dst = distance(pixCoord, cameraPoint);
	if (dst < radius) out_color = vec4(1, 1, 0, 1);
}