#version 420

#define EPS 0.00001

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 alpha_uncertainty;
layout(location = 3) in vec3 features0;
layout(location = 4) in vec3 features1;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform float h;
uniform int usePrevDepth;

out float vPointSize;
out float vAlpha;
out vec3 vInvCov;

//======================================

float sqr(float x) 
{
    return x * x;
}

//======================================

mat2 computeJacobian()
{
    vec3 s_world = vec3(1);
    s_world.y = 0;
    s_world.z = -normal.x / (normal.z + EPS);
    s_world = normalize(s_world);
    
    vec3 t_world = cross(normal, s_world);

    vec4 o = viewMatrix * vec4(position, 1);
    o /= o.w;
    vec3 s = (viewMatrix * vec4(s_world, 0)).xyz;
    vec3 t = (viewMatrix * vec4(t_world, 0)).xyz;

    mat2 jacobian;
    jacobian[0][0] = s.x * o.z - s.z * o.x;
    jacobian[1][0] = -(t.x * o.z - t.z * o.x);
    jacobian[0][1] = -(s.z * o.y - s.y * o.z);
    jacobian[1][1] = t.z * o.y - t.y * o.z;
    return jacobian / sqr(o.z);
}

//======================================

float maxEigen(mat2 A)
{
    float a = A[0][0];
    float b = A[1][1];
    float c = (A[0][1] + A[1][0]) / 2.0;
    float det = sqrt(4 * sqr(c) + sqr(a-b));
    float l1 = (a + b - det) / 2.0;
    float l2 = (a + b + det) / 2.0;
    return max(l1, l2);
}

//======================================

vec3 getPositionWorldSpace(vec2 positionScreenSpace, mat4 proj)
{
    mat4 invCamMatrix = inverse(proj * viewMatrix);
	vec4 positionWorldSpace = invCamMatrix * vec4(positionScreenSpace, 0, 1);
	return positionWorldSpace.xyz / positionWorldSpace.w;
}

//======================================

struct Ray 
{
	vec3 origin;
	vec3 direction;
};

Ray getRay(vec2 fragCoord, mat4 proj)
{
    Ray ray;
    ray.origin = (inverse(viewMatrix) * vec4(0, 0, 0, 1)).xyz;
    vec3 targetPointWorldSpace = getPositionWorldSpace(fragCoord, proj);
    ray.direction = normalize(targetPointWorldSpace - ray.origin);
    return ray;
	
}

//======================================

mat4 getCorrectedProjectionMatrix()
{
    mat4 conv = mat4(1);
    conv[1][1] = -1;
    conv[2][2] = -1;
    return conv * projectionMatrix;
}

//======================================

void main()
{
    // z-offset http://graphics.cs.cmu.edu/projects/objewa/eg2002print.pdf (Fig. 4b)    
    //float zOffset = 0.01; // compost
    //float zOffset = 0.07; // silver vase
    float zOffset = 0.02; // hallway
    //float zOffset = 0.05; // concave bowl

    zOffset = (usePrevDepth != 1) ? zOffset : 1.;

    // compute vanilla projected position
    mat4 proj = getCorrectedProjectionMatrix();
    vec4 unmodifiedPosition = proj * viewMatrix * vec4(position, 1);
    unmodifiedPosition /= unmodifiedPosition.w;

    // offset original position along camera ray
    Ray cameraRay = getRay(unmodifiedPosition.xy, proj);
    vec3 newPos = position + zOffset * cameraRay.direction;
    gl_Position = proj * viewMatrix * vec4(newPos, 1);

    // compute per-point attributes
    vAlpha = alpha_uncertainty.x;
    float uncertainty = alpha_uncertainty.y;
    
    mat2 novel_view_jacobian = h * computeJacobian();

    mat2 vrk = mat2(uncertainty);
    mat2 cov = novel_view_jacobian * vrk * transpose(novel_view_jacobian) + mat2(0.3);
    
    if (determinant(cov) == 0)
    {
        gl_Position = vec4(-1000, 0, 0, 1);
        return;
    }
    
    mat2 inv_cov = inverse(cov);
    vInvCov = vec3(inv_cov[0][0], inv_cov[0][1], inv_cov[1][1]);

    float m = maxEigen(cov);
    vPointSize = 2 * int(ceil(3 * sqrt(m)));
    gl_PointSize = vPointSize;
    
}
