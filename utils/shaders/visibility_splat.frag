#version 420

layout(location=0) out vec4 out_color;

layout (binding = 0) uniform sampler2D prevDepthTexture2D;

uniform int usePrevDepth;

in float vAlpha;
in vec3 vInvCov;
in float vPointSize;


void main()
{	
	vec2 diff = (2 * gl_PointCoord - 1) * vPointSize / 2;
	float exponent = -0.5 * (	vInvCov.x * diff.x * diff.x + 
								2 * vInvCov.y * diff.x * diff.y + 
								vInvCov.z * diff.y * diff.y);
	if (exponent > 0) discard;
	float w = exp(exponent) * vAlpha;
	
	if (usePrevDepth == 1)
	{
		float prevDepth = texelFetch(prevDepthTexture2D, ivec2(gl_FragCoord.xy), 0).x;
		if (gl_FragCoord.z <= prevDepth) discard;
	}
	
	if (w < 0.25) discard;  // more aggressive here than during blending

	float splat_depth =  gl_FragCoord.z;

	out_color = vec4(vec3(splat_depth), 1);
}