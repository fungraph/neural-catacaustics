#version 420

layout (binding = 0) uniform sampler2D inputTexture2D;

in vec2 uv;

out vec4 out_color;

void main()
{	
	out_color = texture(inputTexture2D, uv);
}
