#version 130

in vec2 position;

uniform float scale;

flat out vec4 color;

void main()
{
    gl_Position.xy = scale * (2 * position - vec2(1));
    gl_Position.zw = vec2(0, 1); 
    color = gl_Color;
}
