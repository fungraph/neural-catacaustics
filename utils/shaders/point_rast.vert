#version 420

#define EPS 0.00001

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 alpha_uncertainty;
layout(location = 3) in vec4 features0;
layout(location = 4) in vec3 features1;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform float h;

out float vPointSize;
out vec4 vFeats0;
out vec3 vFeats1;
out float vAlpha;
out vec3 vInvCov;

//======================================

float sqr(float x) 
{
    return x * x;
}

//======================================

mat2 computeJacobian()
{
    vec3 s_world = vec3(1);
    s_world.y = 0;
    s_world.z = -normal.x / (normal.z + EPS);
    s_world = normalize(s_world);
    
    vec3 t_world = cross(normal, s_world);

    vec4 o = viewMatrix * vec4(position, 1);
    o /= o.w;
    vec3 s = (viewMatrix * vec4(s_world, 0)).xyz;
    vec3 t = (viewMatrix * vec4(t_world, 0)).xyz;

    mat2 jacobian;
    jacobian[0][0] = s.x * o.z - s.z * o.x;
    jacobian[1][0] = -(t.x * o.z - t.z * o.x);
    jacobian[0][1] = -(s.z * o.y - s.y * o.z);
    jacobian[1][1] = t.z * o.y - t.y * o.z;
    return jacobian / sqr(o.z);
}

//======================================

float maxEigen(mat2 A)
{
    float a = A[0][0];
    float b = A[1][1];
    float c = (A[0][1] + A[1][0]) / 2.0;
    float det = sqrt(4 * sqr(c) + sqr(a-b));
    float l1 = (a + b - det) / 2.0;
    float l2 = (a + b + det) / 2.0;
    return max(l1, l2);
}

//======================================

void discardPoint()
{
    gl_Position = vec4(-1000, 0, 0, 1);
}

//======================================

void main()
{
    gl_Position = projectionMatrix * viewMatrix * vec4(position, 1);
    gl_Position.yz *= -1;

    vFeats0 = features0;
    vFeats1 = features1;
    vAlpha = alpha_uncertainty.x;
    float uncertainty = alpha_uncertainty.y;
    
    mat2 novel_view_jacobian = h * computeJacobian();

    mat2 vrk = mat2(uncertainty);
    mat2 cov = novel_view_jacobian * vrk * transpose(novel_view_jacobian) + mat2(0.3);
    
    if (determinant(cov) == 0)
    {
        discardPoint();
        return;
    }

    mat2 inv_cov = inverse(cov);
    vInvCov = vec3(inv_cov[0][0], inv_cov[0][1], inv_cov[1][1]);
    
    float m = maxEigen(cov);
    vPointSize = 2 * int(ceil(3 * sqrt(m)));
    gl_PointSize = vPointSize;
}
