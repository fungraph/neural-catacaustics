#version 420

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

out vec3 vNormal;

void main()
{
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1);
    vNormal = (transpose(inverse(modelViewMatrix)) * vec4(normal, 0)).xyz;
}
