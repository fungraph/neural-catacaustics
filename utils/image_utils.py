from random import randint
import torch

def crop_image(img, patch):
    startx, starty, width, height = patch
    b,c,y,x = img.shape
    assert(y>=starty+height and x>=startx+width)
    return img[:, :, starty:starty+height, startx:startx+width]


def crop_center(img, cropx, cropy):
    y,x,c = img.shape
    startx = x//2 - cropx//2
    starty = y//2 - cropy//2
    return img[starty:starty+cropy, startx:startx+cropx, :]


def get_random_crop_coords(img_dims, crop_size):
    x,y = img_dims
    x_crop, y_crop = crop_size
    assert(x >= x_crop and y >= y_crop)
    x0 = randint(0, x - x_crop)
    x1 = x0 + x_crop
    y0 = randint(0, y - y_crop)
    y1 = y0 + y_crop
    return (x0, y0, x1, y1)


def logTransform(image, output_max=1.0):
    c = output_max/torch.log(image.max() + 1)
    return c*torch.log(1+image)


def normalise(vector):
    min_v = torch.min(vector)
    range_v = torch.max(vector) - min_v
    normalised = (vector - min_v) / range_v

    return normalised

def srgb_to_linear(srgb):
    linear = torch.zeros_like(srgb)
    exp = torch.ones_like(srgb)*2.4
    linear[srgb >= 0.04045] = torch.pow((srgb[srgb >= 0.04045] + 0.055)/(1 + 0.055), exp[srgb >= 0.04045])
    linear[srgb < 0.04045] = srgb[srgb < 0.04045]/12.92

    return linear

def linear_to_srgb(linear):
    srgb = torch.zeros_like(linear)
    exp = torch.ones_like(linear)*(1.0/2.4)
    srgb[linear >= 0.0031308] = 1.055 * torch.pow(linear[linear >= 0.0031308], exp[linear >= 0.0031308]) - 0.055
    srgb[linear < 0.0031308] = 12.92*linear[linear < 0.0031308]

    return srgb