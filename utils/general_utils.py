import torch
import numpy as np
import math

class pbnrClamp(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, input, min, max):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        return input.clamp(min=min, max=max)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grad_input = grad_output.clone()
        return grad_input, None, None

def inverse_sigmoid(x):
    return torch.log(x/(1-x))

def inverse_sigmoid_scaled(x, scale):
    return np.log(x/(scale-x))

def batchify(t):
    """
    Converts a tensor from (1, C, H, W) -> (B, C)
    """
    orig_shape = t.shape
    batched_t = t.permute(0, 2, 3, 1).reshape(-1, orig_shape[1])
    return batched_t, orig_shape

def patchify(t, shape):
    """
    Converts a tensor from (B, C) -> (1, C, H, W)
    """
    patch_t = t.permute(1, 0).reshape(shape)
    return patch_t

def PILtoTorch(pil_image, resolution):
    resized_image_PIL = pil_image.resize(resolution)
    resized_image = torch.from_numpy(np.array(resized_image_PIL)) / 255.0
    if len(resized_image.shape) == 3:
        return resized_image.permute(2, 0, 1).unsqueeze(dim=0)
    else:
        return resized_image.unsqueeze(dim=-1).permute(2, 0, 1).unsqueeze(dim=0)

def polar2cart(r, theta, phi):
    return np.array([
         r * np.sin(phi) * np.cos(theta),
         r * np.sin(phi) * np.sin(theta),
         r * np.cos(phi)
    ]).reshape(-1, 3)

def cart2polar(x, y, z):
    v = np.array([x, y, z])
    return np.array([
        np.linalg.norm(v),
        np.arctan2(v[1], v[0]),
        np.arctan2(np.linalg.norm(v[:2]), v[2])
    ]).reshape(-1, 3)

def make_background_from_another_pc(pc, number_of_points, type="sphere"):
    center, radius = pc.get_bounding_sphere()
    theta = 2*math.pi*np.random.uniform(size=(number_of_points, 1))
    phi = np.arccos(1 - 2*np.random.uniform(size=(number_of_points, 1)))
    if type=="sphere":
        r = radius.item()*np.ones((number_of_points, 1))
    else:
        # Normal distibution from radius -> 2*radius
        r = 2*radius.item()*np.abs(np.random.normal(scale=1.0, size=(number_of_points, 1))) + radius.item()

    xyz = np.concatenate((r * np.sin(phi) * np.cos(theta),
                          r * np.sin(phi) * np.sin(theta),
                          r * np.cos(phi)), axis=1)

    bg_pcd = o3d.geometry.PointCloud()
    bg_pcd.points = o3d.utility.Vector3dVector(xyz)
    bg_pcd.normals = o3d.utility.Vector3dVector(-xyz/np.linalg.norm(-xyz, axis=1, keepdims=True))
    bg_pcd.paint_uniform_color(np.array([0.5, 0.5, 0.5]))

    return bg_pcd

def sample_mesh_pytorch3d(path, num_points):
    import pytorch3d
    from pytorch3d.ops import sample_points_from_meshes
    from pytorch3d.structures import Meshes
    from pytorch3d.io import load_obj, save_obj
    import open3d as o3d

    verts, faces, _ = load_obj(path)
    trg_mesh = Meshes(verts=[verts], faces=[faces.verts_idx])
    points, normals = sample_points_from_meshes(trg_mesh, num_points, return_normals=True)

    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points.squeeze().detach().numpy())
    pcd.normals = o3d.utility.Vector3dVector(normals.squeeze().detach().numpy())
    pcd.paint_uniform_color(np.array([0.5, 0.5, 0.5]))

    return pcd

def sample_mesh_o3d(path, num_points):
    import open3d as o3d

    mesh = o3d.io.read_triangle_mesh(path)
    pcd = mesh.sample_points_uniformly(num_points, use_triangle_normal=True)
    pcd.paint_uniform_color(np.array([0.5, 0.5, 0.5]))
    return pcd