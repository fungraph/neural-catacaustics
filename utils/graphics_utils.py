import torch
import math
import numpy as np

def geom_transform_vectors(vectors, transf_matrix):

    P, _ = vectors.shape
    zeros = torch.zeros(P, 1, dtype=vectors.dtype, device=vectors.device)
    vectors_hom = torch.cat([vectors, zeros], dim=1)
    vectors_out = torch.matmul(vectors_hom, transf_matrix)

    return (vectors_out[..., :3]).squeeze(dim=0)

def geom_transform_points(points, transf_matrix):

    P, _ = points.shape
    ones = torch.ones(P, 1, dtype=points.dtype, device=points.device)
    points_hom = torch.cat([points, ones], dim=1)
    points_out = torch.matmul(points_hom, transf_matrix)

    denom = points_out[..., 3:] + 0.0000001
    return (points_out[..., :3] / denom).squeeze(dim=0)

def geom_transform_Zviewspace(points, full_proj_transform, world2view):
    proj_space = geom_transform_points(points, full_proj_transform)
    view_space = geom_transform_points(points, world2view)
    proj_space[..., 2] = view_space[..., 2]

    return proj_space

def convert_Zworld_to_Zndc(Zworld, cam):
    c = cam.projection_matrix[0, 2, 2]
    d = cam.projection_matrix[0, 3, 2]
    return (c * Zworld + d) / Zworld

def projectToCamera(hom_cloud, cam):
    full_proj_transform = cam.full_proj_transform
    pts_projected = hom_cloud.bmm(full_proj_transform)
    pts_projected_normalised = pts_projected / (pts_projected[..., 3:]+ 0.0000001)

    view_transform = cam.world_view_transform
    points_viewspace = hom_cloud.bmm(view_transform)
    points_viewspace = points_viewspace / points_viewspace[..., 3:]

    pts_projected_normalised[..., 2] = points_viewspace[..., 2]
    return pts_projected_normalised.squeeze(0)[:, :3]

def getWorld2View(R, t):
    Rt = np.zeros((1, 4, 4))
    Rt[:, :3, :3] = R.transpose()
    Rt[:, :3, 3] = t
    Rt[:, 3, 3] = 1.0
    return np.float32(Rt)

def getWorld2View2(R, t, translate=np.array([.0, .0, .0]), scale=1.0):
    Rt = np.zeros((1, 4, 4))
    Rt[:, :3, :3] = R.transpose()
    Rt[:, :3, 3] = t
    Rt[:, 3, 3] = 1.0

    C2W = np.linalg.inv(Rt)
    cam_center = C2W[0, :3, 3]
    cam_center = (cam_center + translate) * scale
    C2W[0, :3, 3] = cam_center
    Rt = np.linalg.inv(C2W)
    return np.float32(Rt)

def getPatchProjectionMatrix(znear, zfar, top, bottom, right, left):
    top = top*znear  # pyre-ignore[16]
    bottom = bottom * znear  # pyre-ignore[16]
    right = right * znear  # pyre-ignore[16]
    left = left * znear

    P = torch.zeros( 1, 4, 4)

    #ones = torch.ones((self._N))

    # NOTE: In OpenGL the projection matrix changes the handedness of the
    # coordinate frame. i.e the NDC space postive z direction is the
    # camera space negative z direction. This is because the sign of the z
    # in the projection matrix is set to -1.0.
    # In pytorch3d we maintain a right handed coordinate system throughout
    # so the so the z sign is 1.0.
    z_sign = 1.0

    P[:, 0, 0] = 2.0 * znear / (right - left)
    P[:, 1, 1] = 2.0 * znear / (top - bottom)
    P[:, 0, 2] = (right + left) / (right - left)
    P[:, 1, 2] = (top + bottom) / (top - bottom)
    P[:, 3, 2] = z_sign

    # NOTE: This part of the matrix is for z renormalization in OpenGL
    # which maps the z to [-1, 1]. This won't work yet as the torch3d
    # rasterizer ignores faces which have z < 0.
    # P[:, 2, 2] = z_sign * (far + near) / (far - near)
    # P[:, 2, 3] = -2.0 * far * near / (far - near)
    # P[:, 3, 2] = z_sign * torch.ones((N))

    # NOTE: This maps the z coordinate from [0, 1] where z = 0 if the point
    # is at the near clipping plane and z = 1 when the point is at the far
    # clipping plane. This replaces the OpenGL z normalization to [-1, 1]
    # until rasterization is changed to clip at z = -1.
    P[:, 2, 2] = z_sign * zfar / (zfar - znear)
    P[:, 2, 3] = -(zfar * znear) / (zfar - znear)

    return P


def getProjectionMatrix(znear, zfar, fovX, fovY):
    tanHalfFovY = math.tan((fovY / 2))
    tanHalfFovX = math.tan((fovX / 2))

    top = tanHalfFovY * znear
    bottom = -top
    right = tanHalfFovX * znear
    left = -right

    P = torch.zeros( 1, 4, 4)

    # NOTE: In OpenGL the projection matrix changes the handedness of the
    # coordinate frame. i.e the NDC space postive z direction is the
    # camera space negative z direction. This is because the sign of the z
    # in the projection matrix is set to -1.0.
    # In pytorch3d we maintain a right handed coordinate system throughout
    # so the so the z sign is 1.0.
    z_sign = 1.0

    P[:, 0, 0] = 2.0 * znear / (right - left)
    P[:, 1, 1] = 2.0 * znear / (top - bottom)
    P[:, 0, 2] = (right + left) / (right - left)
    P[:, 1, 2] = (top + bottom) / (top - bottom)
    P[:, 3, 2] = z_sign

    # NOTE: This part of the matrix is for z renormalization in OpenGL
    # which maps the z to [-1, 1]. This won't work yet as the torch3d
    # rasterizer ignores faces which have z < 0.
    # P[:, 2, 2] = z_sign * (far + near) / (far - near)
    # P[:, 2, 3] = -2.0 * far * near / (far - near)
    # P[:, 3, 2] = z_sign * torch.ones((N))

    # NOTE: This maps the z coordinate from [0, 1] where z = 0 if the point
    # is at the near clipping plane and z = 1 when the point is at the far
    # clipping plane. This replaces the OpenGL z normalization to [-1, 1]
    # until rasterization is changed to clip at z = -1.
    P[:, 2, 2] = z_sign * zfar / (zfar - znear)
    P[:, 2, 3] = -(zfar * znear) / (zfar - znear)

    return P

def fov2focal(fov, pixels):
    return pixels / (2 * math.tan(fov / 2))

def focal2fov(focal, pixels):
    return 2*math.atan(pixels/(2*focal))

def sensor_size(fov, pixels):
    return 2*fov2focal(fov, pixels)*math.tan(fov/2.0)

def unprojectCameraDepth(camera, patch, depth_map):
    patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch
    if not hasattr(camera, "point_grid"):
        pixel_grid = getNormalisedImageGrid(patch_size_y, patch_size_x)[:, patch_origin_y:patch_origin_y + patch_size_y,
                                                                           patch_origin_x:patch_origin_x + patch_size_x].to("cuda")
    else:
        pixel_grid = camera.point_grid[:, patch_origin_y:patch_origin_y + patch_size_y,
                                          patch_origin_x:patch_origin_x + patch_size_x].to("cuda")

    points = depth_map*pixel_grid
    pointsA = points.permute(0,2,3,1).view(points.shape[2]*points.shape[3], points.shape[1])

    K = camera.projection_matrix[:, :3, :3]
    K[0,2,2] = 1.0 # This removes the z component of the trasnformation because z is already at view space.
    Kinv = K.inverse()
    points_proj_inv = torch.matmul(pointsA, Kinv).squeeze(0)
    points_wc = geom_transform_points(points_proj_inv, camera.world_view_transform.inverse())

    return points_wc

def getImageGrid(height, width):
    x_axis = torch.linspace(0, width - 1, width)
    y_axis = torch.linspace(0, height - 1, height)
    yv, xv = torch.meshgrid(y_axis, x_axis)
    return torch.stack((xv, yv), dim=-1)

def getNormalisedImageGrid(height, width):
    x_axis = torch.linspace(-1.0, 1.0-2.0/width, width) + 2.0/(2.0*width)
    y_axis = torch.linspace(-1.0, 1.0-2.0/height, height) + 2.0/(2.0*height)
    yv, xv = torch.meshgrid(y_axis, x_axis)
    return torch.stack((xv, yv, torch.ones_like(yv)), dim=0)

def eigen(A):
    a = A[:, 0, 0].unsqueeze(1)
    c = ((A[:, 0, 1] + A[:, 1, 0])/2.0).unsqueeze(1)
    #c = A[:, 1, 0]
    b = A[:, 1, 1].unsqueeze(1)

    det = torch.sqrt(4.0*c*c + (a-b)*(a-b))

    l1 = (a + b - det)/2.0
    l2 = (a + b + det)/2.0

    v1 = torch.cat((2*c, b - a + det), dim=1)
    v2 = torch.cat((2*c, b - a - det), dim=1)
    #v1 = torch.cat((v11.unsqueeze(1), torch.ones_like(v11).unsqueeze(1)), dim=1)
    #v2 = torch.cat((v21.unsqueeze(1), torch.ones_like(v21).unsqueeze(1)), dim=1)
    v = torch.cat(((v2/(v2.norm(dim=1, keepdim=True)+0.0000001)).unsqueeze(2), (v1/(v1.norm(dim=1, keepdim=True)+0.0000001)).unsqueeze(2)), dim=2)

    return torch.cat((l1, l2), dim=1), v

def inverse(A):
    inv = torch.zeros_like(A)
    inv[:, 0, 0] = A[:, 1, 1]
    inv[:, 1, 1] = A[:, 0, 0]
    inv[:, 0, 1] = -A[:, 0, 1]
    inv[:, 1, 0] = -A[:, 1, 0]

    return inv/(A.det().unsqueeze(1).unsqueeze(2) + 0.0000001)

def computeJacobian(point_cloud, normal, camera):
    s_world = torch.ones_like(normal, device="cuda")
    s_world[:, 1] = 0.0
    s_world[:, 2] = -normal[:, 0] / (normal[:, 2] + 0.0000001)
    s_world = s_world / s_world.norm(dim=1, keepdim=True)

    t_world_b = normal.cross(s_world, dim=1)
    t_world = t_world_b / t_world_b.norm(dim=1, keepdim=True)

    o = geom_transform_points(point_cloud, camera.world_view_transform)
    s = geom_transform_vectors(s_world, camera.world_view_transform)
    t = geom_transform_vectors(t_world, camera.world_view_transform)

    jacobian = torch.zeros(o.shape[0], 2, 2, device="cuda")
    jacobian[:, 0, 0] = s[:, 0] * o[:, 2] - s[:, 2] * o[:, 0]
    jacobian[:, 0, 1] = -(t[:, 0] * o[:, 2] - t[:, 2] * o[:, 0])
    jacobian[:, 1, 0] = -(s[:, 2] * o[:, 1] - s[:, 1] * o[:, 2])
    jacobian[:, 1, 1] = t[:, 2] * o[:, 1] - t[:, 1] * o[:, 2]


    # Fixes scaling issue when using cropped camera.
    #test_scale_x = (camera.image_width/2.0)/camera.projection_matrix[0,0,0]
    #test_scale_x = camera.projection_matrix[0, 0, 0]
    #test_scale_y = (camera.image_height/2.0)/camera.projection_matrix[0,1,1]

    #test_scale_x = torch.tensor([1.0]).cuda()
    #test_scale_y = torch.tensor([1.0]).cuda()


    #scale = torch.tensor([[test_scale_x, 0.0], [0.0, test_scale_y]], device="cuda")
    #jacobian = torch.matmul(jacobian, scale)
    jacobian = jacobian/(o[:, 2] * o[:, 2]).unsqueeze(dim=1).unsqueeze(dim=1)

    return jacobian


def percentile(t, q) :
    """
    Return the ``q``-th percentile of the flattened input tensor's data.

    CAUTION:
     * Needs PyTorch >= 1.1.0, as ``torch.kthvalue()`` is used.
     * Values are not interpolated, which corresponds to
       ``numpy.percentile(..., interpolation="nearest")``.

    :param t: Input tensor.
    :param q: Percentile to compute, which must be between 0 and 100 inclusive.
    :return: Resulting value (scalar).
    """
    # Note that ``kthvalue()`` works one-based, i.e. the first sorted value
    # indeed corresponds to k=1, not k=0! Use float(q) instead of q directly,
    # so that ``round()`` returns an integer, even if q is a np.float32.
    k = 1 + round(.01 * float(q) * (t.numel() - 1))
    result = t.view(-1).kthvalue(k).values
    return result


def RotX(rad):
    return np.array([[1.0, 0.0, 0.0, 0.0],
                     [0.0, math.cos(rad), -math.sin(rad), 0.0],
                     [0.0, math.sin(rad), math.cos(rad), 0.0],
                     [0.0, 0.0, 0.0, 1.0]])


def RotY(rad):
    return np.array([[math.cos(rad), 0.0, math.sin(rad), 0.0],
                     [0.0, 1.0, 0.0, 0.0],
                     [-math.sin(rad), 0.0, math.cos(rad), 0.0],
                     [0.0, 0.0, 0.0, 1.0]]).astype(np.float32)


def RotZ(rad):
    return np.array([[math.cos(rad), -math.sin(rad), 0.0, 0.0],
                     [math.sin(rad), math.cos(rad), 0.0, 0.0],
                     [0.0, 0.0, 1.0, 0.0],
                     [0.0, 0.0, 0.0, 1.0]]).astype(np.float32)


def getDirRays3(camera, sigma=0.0):
    # https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/generating-camera-rays
    pxpy = getImageGrid(camera.image_height, camera.image_width).cuda() + 0.5
    aspect = camera.image_width/camera.image_height
    px = (1.0 - 2*pxpy[:,:,0:1]/camera.image_width) * aspect * math.tan(camera.FoVy/2.0)
    py = (1.0 - 2*pxpy[:,:,1:2]/camera.image_height) * math.tan(camera.FoVy/2.0)

    if sigma>0.0:
        r_x = torch.randn_like(px)*sigma
        r_y = torch.randn_like(py)*sigma
    else:
        r_x = torch.zeros_like(px)
        r_y = torch.zeros_like(py)
    P = torch.cat((px+r_x, py+r_y, -torch.ones_like(py)), dim=2)

    O_prime = camera.camera_center.view(1,3,1,1)
    P_prime = geom_transform_points(P.view(-1,3),
                                    camera.world_view_transform.inverse()).permute(1,0).view(1,3,camera.image_height, camera.image_width)
    dirworld = P_prime - O_prime
    return dirworld/dirworld.norm(dim=1, keepdim=True)


def projectEnvmap(camera, envmap, patch):
    dirrays = getDirRays3(camera)[:,:, patch[1]:patch[1]+patch[3], patch[0]:patch[0]+patch[2]]

    theta = torch.acos(dirrays[:,2:3,...])/(math.pi/2.0) - 1.0
    phi = torch.fmod(torch.atan2(dirrays[:, 1:2, ...], dirrays[:, 0:1, ...]) / math.pi + 2.0, 2.0) - 1.0

    grid = torch.cat((phi, -theta), dim=1).permute(0,2,3,1)
    projected_envmap = torch.nn.functional.grid_sample(envmap, grid,
                                                       mode="bilinear", padding_mode="reflection",
                                                       align_corners=False)

    return projected_envmap




def create_quad_plane_mesh(res):

    # vertices
    x = np.linspace(0, 1, res[0]+1)
    y = np.linspace(0, 1, res[1]+1)
    vertices = np.stack(np.meshgrid(x, y), axis=-1).reshape(-1, 2).astype(np.float32)

    # connectivity
    tris = []
    for i in range(res[1]):
        for j in range(res[0]):
            a = i * (res[0] + 1) + j
            b = a + 1
            d = a + (res[0] + 1)
            c = d + 1
            tris.append([a, b, c])
            tris.append([a, c, d])
    tris = np.array(tris)

    return vertices, tris


def exportMeshAsObj(mesh, filename):
    with open(filename, 'w') as file:
        for v in mesh.vertices:
            line = "v " + str(v[0]) + " " + str(v[1]) + " " + str(v[2])
            file.write(line + "\n")
        for t in mesh.tris:
            line = "f " + str(t[0] + 1) + " " + str(t[1] + 1) + " " + str(t[2] + 1)
            file.write(line + "\n")


# find 2 orthogonal vectors which are also orthogonal to the input vector
def completeOrthogonalBasis(v1, up=None):
    v2 = np.random.randn(3) if up is None else up
    v2 -= v2.dot(v1) * v1
    v2 /= np.linalg.norm(v2)
    v3 = np.cross(v1, v2)
    return np.array([v1, v2, v3])