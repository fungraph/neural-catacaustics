import torch
import uuid
from torch.utils.tensorboard import SummaryWriter
from random import randint
import random
from model_defs.Loss_Functions import l1_loss, cata_loss, tv_reg, ssim
from utils.system_utils import mkdir_p
from utils.image_utils import crop_image
from renderer.render_point_cloud import render_scene, render_scene_diffuse_only
import os
import subprocess
import sys
from datetime import datetime
from scene_loaders.ibr_scene import Scene
from arguments.parse_args import get_args
import math

old_f = sys.stdout
class F:
    def write(self, x):
        if x.endswith("\n"):
            old_f.write(x.replace("\n", " [{}]\n".format(str(datetime.now().strftime("%d/%m %H:%M:%S")))))
        else:
            old_f.write(x)

    def flush(self):
        old_f.flush()
sys.stdout = F()

def render_viewpoint(viewpoint_camera, scene, iteration, patch=None, print_stats=False, diffuse_only=False, w_dampen=1.0):
    if patch==None:
        patch = (0, 0, viewpoint_camera.image_width, viewpoint_camera.image_height)
    if diffuse_only:
        view, diffuse_render, environment_mask, viewspace_points_tensor, diffuse_filter = render_scene_diffuse_only(viewpoint_camera, scene, patch)
        specular_render = torch.ones_like(view)
        mask = torch.ones_like(view)
        return view, diffuse_render, specular_render, mask, None, None, viewspace_points_tensor, diffuse_filter
    else:
        view, diffuse_render, specular_render, mask, specular_environment_mask, filter, viewspace_points_tensor, diffuse_filter = render_scene(viewpoint_camera, scene, iteration, patch, w_dampen=w_dampen)
        return view, diffuse_render, specular_render, mask, specular_environment_mask, filter, viewspace_points_tensor, diffuse_filter



device = torch.device("cuda:0")
torch.cuda.set_device(device)
torch.manual_seed(0)
random.seed(0)

args = get_args()
torch.autograd.set_detect_anomaly(args.detect_anomaly)

if os.getenv('OAR_JOB_ID'):
    unique_str=os.getenv('OAR_JOB_ID')
else:
    unique_str = str(uuid.uuid4())
tensorboard_folder = os.path.join("./tensorboard_3d/", args.output_path + unique_str[0:10])
assert not os.path.exists(tensorboard_folder), ("Output folder already exists!")
tb_writer = SummaryWriter(tensorboard_folder)

print("Output Folders:")
print(" * {}".format(tensorboard_folder))

with open(os.path.join(tensorboard_folder, "run_info"), 'w') as git_log_f:
    git_log_f.write(" ".join(sys.argv))
    subprocess.run(["git", "log", "-n", "1"], stdout=git_log_f)
    subprocess.run(["git", "diff"], stdout=git_log_f)
with open(os.path.join(tensorboard_folder, "cfg_args"), 'w') as cfg_log_f:
    cfg_log_f.write(str(args))

iter_backward = 20
patch_size_og = int(args.patch_size)
resolution_scale = float(args.resolution_scale)
scales = [resolution_scale]
if resolution_scale>1:
    rs = resolution_scale
    while (int(rs / 2.0) != 1):
        rs = int(rs / 2.0)
        scales.append(float(rs))
    scales.append(1.0)
print("Resolution scales : ", scales)
scene = Scene(args, resolution_scales=scales) #, load_input_data=False)
#scene.debugView(resolution_scale)

# mlp = scene.warp_field_mlp
# test_points = torch.tensor([[0.5, 0.3, 1.2], [1, 2, 3]]).cuda()
# test_camera = torch.tensor([[0.2, 0.3, 0.4], [0.2, 0.3, 0.4]]).cuda()
# mlp_out = mlp([test_points, test_camera])
# exit()

optimizer_envmap = torch.optim.Adam([
    {'params': [scene.environment_map], 'lr': args.feature_lr}
], lr=0.0, eps=1e-15)

optimizer_mlp = torch.optim.Adam(scene.warp_field_mlp.parameters(), lr=args.mlp_lr, weight_decay=args.mlp_weight_decay)
optimizer_view_renderer = torch.optim.Adam(scene.view_renderer.parameters(), lr=args.cnns_lr)

test_loss_logs = []

iteration = 1
gamma = 1.0
flag_gt_logged = False
viewpoint_stack = scene.getTrainCameras(resolution_scale).copy()
start_iter_timer = torch.cuda.Event(enable_timing=True)
stop_iter_timer = torch.cuda.Event(enable_timing=True)


def w_dampen_factor(iteration):
    if not args.progressive_catacaustics:
        return 1.0
    start_dampen = 1e8 #100000
    end_dampen = start_dampen * 2.5
    if iteration < start_dampen:
        return 1.0
    if iteration < end_dampen:
        return 1. - ((iteration - start_dampen) / (end_dampen - start_dampen))
    else:
        return 0.
    
while True:
    start_iter_timer.record()
    if iteration==int(args.total_iterations):
        break

    w_dampen = w_dampen_factor(iteration)

    viewpoint_idx = randint(0, len(viewpoint_stack)-1)
    viewpoint_cam = viewpoint_stack.pop(viewpoint_idx)

    patch_size = int(patch_size_og/resolution_scale)
    patch = viewpoint_cam.getRandomPatch(patch_size, polytope_only=args.polytope_only)

    image, diffuse_render, specular_render, predicted_mask, specular_environment_mask, filter, viewspace_point_tensor, diffuse_filter = render_viewpoint(viewpoint_cam, scene, iteration, patch, diffuse_only=args.diffuse_only, w_dampen=w_dampen)

    gt_image = viewpoint_cam.original_image.to(device)
    gt_image = crop_image(gt_image, patch)
    gt_alpha = crop_image(viewpoint_cam.gt_alpha_mask, patch)
    if specular_environment_mask is not None:
        polytope_mask = crop_image(viewpoint_cam.polytope_mask.cuda(), patch)
        LR_mask = l1_loss(predicted_mask, polytope_mask)
        LR_specular_mask = l1_loss((1.0 - specular_environment_mask)*polytope_mask, polytope_mask)
        LR_tv = tv_reg(predicted_mask)
    else:
        LR_mask = torch.tensor([0.0], device="cuda")
        LR_specular_mask = torch.tensor([0.0], device="cuda")
        LR_tv = torch.tensor([0.0], device="cuda")
    if filter is not None and args.lambda_cata>0.0:
        Lcata = cata_loss(scene.warp_field_mlp, viewpoint_cam, filter)
    else:
        Lcata = torch.tensor([0.0], device="cuda")

    Ll1 = l1_loss(image*gt_alpha, gt_image)
    loss = (1.0 - args.lambda_dssim)*Ll1 + args.lamda_polytope*LR_mask + \
           args.lamda_specular*LR_specular_mask + \
           args.lambda_cata*Lcata + \
           args.lambda_tv*LR_tv + \
           (args.lambda_dssim)*(1.0-ssim(image*gt_alpha, gt_image))
    loss.backward()

    if viewspace_point_tensor is not None:
        scene.diffuse_point_cloud.gradient_accum += torch.norm(viewspace_point_tensor.grad[:,:2], dim=-1, keepdim=True)
        scene.diffuse_point_cloud.denom[diffuse_filter] += 1
    if iteration % int(args.iter_densify) == 0 and resolution_scale == 1.0 and not args.no_densify:
        tb_writer.add_histogram("diffuse_scene/xyz_grad_norm", torch.norm(scene.diffuse_point_cloud.get_xyz_grads(), dim=-1), iteration)
        #scene.diffuse_point_cloud.densify_gradient(scene.diffuse_point_cloud.get_xyz_grads(), args.densify_grad_threshold)
        grads = scene.diffuse_point_cloud.gradient_accum/scene.diffuse_point_cloud.denom
        grads[grads.isnan()] = 0.0
        tb_writer.add_histogram("diffuse_scene/viewspace_xyz_grad_norm", torch.norm(grads, dim=-1), iteration)
        scene.diffuse_point_cloud.densify_gradient(grads, args.densify_grad_threshold)

    if iteration % iter_backward == 0:
        # We do the optimization step once for all the scales
        scene.diffuse_point_cloud.optimizer.step()
        if not args.diffuse_only:
            scene.specular_point_cloud.optimizer.step()
        optimizer_envmap.step()
        optimizer_mlp.step()
        optimizer_view_renderer.step()

        # Zero out the gradients
        scene.diffuse_point_cloud.optimizer.zero_grad(set_to_none=True)
        if not args.diffuse_only:
            scene.specular_point_cloud.optimizer.zero_grad(set_to_none=True)
        optimizer_envmap.zero_grad(set_to_none=True)
        optimizer_mlp.zero_grad()
        optimizer_view_renderer.zero_grad()
    stop_iter_timer.record()

    # tb_writer.add_scalars('train_loss_patches', {"total_loss": loss.item(), "l1_loss":  Ll1.item()}, iteration)
    tb_writer.add_scalar('train_loss_patches/l1_loss', Ll1.item(), iteration)
    tb_writer.add_scalar('train_loss_patches/lR_mask_loss', LR_mask.item(), iteration)
    tb_writer.add_scalar('train_loss_patches/lR_specular_mask', LR_specular_mask.item(), iteration)
    tb_writer.add_scalar('train_loss_patches/cata_loss', Lcata.item(), iteration)
    tb_writer.add_scalar('train_loss_patches/total_loss', loss.item(), iteration)
    tb_writer.add_scalar('iter_time', start_iter_timer.elapsed_time(stop_iter_timer), iteration)
    tb_writer.add_scalar('w_dampen', w_dampen, iteration)

    with torch.no_grad():
        if not viewpoint_stack:
            viewpoint_stack = scene.getTrainCameras(resolution_scale).copy()
        if iteration%args.iter_save==0:
            print("[ITER {}]Saving Model...".format(iteration))
            scene.save(tensorboard_folder, iteration)
        if iteration%5000==0:
            scene.diffuse_point_cloud.reset_uncertainties()

        start_decay = 100_000
        if iteration >= start_decay:
            decay = 0.000002
            lr = args.mlp_lr*math.exp(-decay * (iteration - start_decay))
            for g in optimizer_mlp.param_groups:
                g['lr'] = lr 
            tb_writer.add_scalar('mlp_lr', lr, iteration)

        if iteration % int(args.iter_densify) == 0:
            if resolution_scale > 1.0:
                lr_flag = False
                resolution_scale = float(int(resolution_scale / 2.0))
                if resolution_scale == 1:
                    args.iter_densify = 2000
                    lr_flag = True
                scene.diffuse_point_cloud.densify_fast(resolution_scale=resolution_scale,
                                                       normals=True,
                                                       propagate_position=True,
                                                       lr_flag=lr_flag)
                if not scene.diffuse_only:
                    scene.specular_point_cloud.densify_fast(resolution_scale=resolution_scale,
                                                            lr_flag=lr_flag)
                print("Adjusting resolution, scale {}".format(resolution_scale))
                viewpoint_stack = scene.getTrainCameras(resolution_scale).copy()

        # Validation
        if (iteration%args.iter_validation == 0 or iteration==1 or (iteration % int(args.iter_densify)==int(args.iter_densify)-1 and resolution_scale>1)) and not args.skip_validation:
            tb_writer.add_histogram("diffuse_scene/xyz_histogram", scene.diffuse_point_cloud.global_xyz, iteration)
            tb_writer.add_histogram("diffuse_scene/uncertainty_histogram", scene.diffuse_point_cloud.global_uncertainty, iteration)
            tb_writer.add_histogram("diffuse_scene/features_histogram", scene.diffuse_point_cloud.global_features, iteration)
            tb_writer.add_histogram("diffuse_scene/alpha_histogram", scene.diffuse_point_cloud.global_alpha, iteration)
            # tb_writer.add_histogram("diffuse_scene/xyz_grad_norm", torch.norm(scene.diffuse_point_cloud.global_xyz.grad, dim=-1), iteration)

            if not args.diffuse_only:
                tb_writer.add_histogram("specular_scene/xyz_histogram", scene.specular_point_cloud.global_xyz, iteration)
                tb_writer.add_histogram("specular_scene/uncertainty_histogram", scene.specular_point_cloud.global_uncertainty, iteration)
                tb_writer.add_histogram("specular_scene/features_histogram", scene.specular_point_cloud.global_features, iteration)
                tb_writer.add_histogram("specular_scene/alpha_histogram", scene.specular_point_cloud.global_alpha, iteration)

            scene.view_renderer.plot_histogram(tb_writer, "model_view_renderer/", iteration)
            scene.warp_field_mlp.plot_histogram(tb_writer, "model_warp_field_mlp/", iteration)

            torch.cuda.empty_cache()
            total_loss = 0.0
            total_LR_mask = 0.0
            total_LR_specular_mask = 0.0
            total_L1 = 0.0
            total_L1_polyreg_only = 0.0
            start_iter_timer.record()
            for idx, test_viewpoint in enumerate(scene.getValidationCameras(1.0)):
                torch.cuda.empty_cache()
                image, diffuse_render, specular_render, predicted_mask, specular_environment_mask, _, _, _ = render_viewpoint(test_viewpoint, scene,
                                                                                                                           iteration,
                                                                                                                           print_stats=True,
                                                                                                                           diffuse_only=args.diffuse_only,
                                                                                                                           w_dampen=w_dampen)
                gt_image = test_viewpoint.original_image.to("cuda")

                Ll1 = l1_loss(image*test_viewpoint.gt_alpha_mask, gt_image)
                LR_mask = torch.tensor([0.0], device="cuda")
                if specular_environment_mask is not None:
                    polytope_mask = test_viewpoint.polytope_mask.cuda()
                    if scene.use_mask:
                        LR_mask = l1_loss(predicted_mask, polytope_mask)
                    LR_specular_mask = l1_loss((1.0 - specular_environment_mask)*polytope_mask, polytope_mask)
                    Ll1_polyreg_only = l1_loss(image * test_viewpoint.gt_alpha_mask * polytope_mask, gt_image*polytope_mask)
                else:
                    LR_specular_mask = torch.tensor([0.0], device="cuda")
                    Ll1_polyreg_only = torch.tensor([0.0], device="cuda")

                loss = Ll1 + args.lamda_polytope*LR_mask + args.lamda_specular*LR_specular_mask

                total_loss += loss.item()/(len(scene.getValidationCameras(resolution_scale)))
                total_LR_mask += LR_mask.item()/(len(scene.getValidationCameras(resolution_scale)))
                total_LR_specular_mask += LR_specular_mask.item()/(len(scene.getValidationCameras(resolution_scale)))
                total_L1 += Ll1.item()/(len(scene.getValidationCameras(resolution_scale)))
                total_L1_polyreg_only += Ll1_polyreg_only.item()/(len(scene.getValidationCameras(resolution_scale)))
                if idx < 5:
                    tb_writer.add_images("test_view_{}/neural_render".format(test_viewpoint.image_name), torch.clamp(image, 0.0, 1.0),
                                         global_step=iteration)
                    if not flag_gt_logged:
                        tb_writer.add_images("test_view_{}/ground_truth".format(test_viewpoint.image_name),
                                             torch.clamp(gt_image, 0.0, 1.0), global_step=iteration)
                        if specular_environment_mask is not None:
                            tb_writer.add_images("test_view_{}/polytope_mask".format(test_viewpoint.image_name),
                                                 polytope_mask, global_step=iteration)

                    tb_writer.add_images("test_view_{}/diffuse_render".format(test_viewpoint.image_name),
                                         torch.clamp(diffuse_render[:,:3,:,:], 0.0, 1.0),
                                         global_step=iteration)
                    if not args.diffuse_only:
                        tb_writer.add_images("test_view_{}/specular_render".format(test_viewpoint.image_name),
                                            torch.clamp(specular_render[:,:3,:,:], 0.0, 1.0),
                                            global_step=iteration)
                    if scene.use_mask:
                        tb_writer.add_images("test_view_{}/predicted_mask".format(test_viewpoint.image_name),
                                             predicted_mask,
                                             global_step=iteration)

            # tb_writer.add_scalars('test_loss_viewpoint', {"total_loss": total_loss, "l1_loss": total_L1}, iteration)
            tb_writer.add_scalar('test/loss_viewpoint - total_loss', total_loss, iteration)
            tb_writer.add_scalar('test/loss_viewpoint - l1_loss', total_L1, iteration)
            tb_writer.add_scalar('test/loss_viewpoint - l1_polytope_reg_loss', total_L1_polyreg_only, iteration)
            tb_writer.add_scalar('test/loss_viewpoint - LR_mask', total_LR_mask, iteration)
            tb_writer.add_scalar('test/loss_viewpoint - LR_specular_mask', total_LR_specular_mask, iteration)

            tb_writer.add_scalar('total_diffuse_points', scene.diffuse_point_cloud.global_xyz.shape[0], iteration)

            test_loss_logs.append(total_loss)
            stop_iter_timer.record()
            torch.cuda.synchronize()
            print("[ITER {}] Test Loss {} [Elapsed: {}]".format(iteration, total_loss, start_iter_timer.elapsed_time(stop_iter_timer)))

            total_loss = 0.0
            total_L1 = 0.0
            total_LR_specular_mask = 0.0
            total_LR_mask = 0.0
            total_L1_polyreg_only = 0.0
            validation_cameras = [scene.getTrainCameras(1.0)[idx] for idx in [5, 10, 15, 20, 25]]
            start_iter_timer.record()
            for idx, train_viewpoint in enumerate(validation_cameras):
                torch.cuda.empty_cache()
                image, diffuse_render, specular_render, predicted_mask, specular_environment_mask, _, _, _ = render_viewpoint(train_viewpoint, scene, iteration,
                                                                                diffuse_only=args.diffuse_only, w_dampen=w_dampen)
                gt_image = train_viewpoint.original_image.to("cuda")

                Ll1 = l1_loss(image*train_viewpoint.gt_alpha_mask, gt_image)
                LR_mask = torch.tensor([0.0], device="cuda")
                if specular_environment_mask is not None:
                    polytope_mask = train_viewpoint.polytope_mask.cuda()
                    if scene.use_mask:
                        LR_mask = l1_loss(predicted_mask, polytope_mask)
                    LR_specular_mask = l1_loss((1.0 - specular_environment_mask)*polytope_mask, polytope_mask)
                    Ll1_polyreg_only = l1_loss(image * train_viewpoint.gt_alpha_mask * polytope_mask, gt_image*polytope_mask)
                else:
                    LR_specular_mask = torch.tensor([0.0], device="cuda")
                    Ll1_polyreg_only = torch.tensor([0.0], device="cuda")

                loss = Ll1 + args.lamda_polytope*LR_mask + args.lamda_specular*LR_specular_mask

                total_loss += loss.item()/(len(validation_cameras))
                total_LR_mask += LR_mask.item()/(len(validation_cameras))
                total_LR_specular_mask += LR_specular_mask.item()/(len(validation_cameras))
                total_L1 += Ll1.item()/(len(validation_cameras))
                total_L1_polyreg_only += Ll1_polyreg_only.item()/(len(scene.getTrainCameras(resolution_scale)))

                tb_writer.add_images("train_view_{}/neural_render".format(train_viewpoint.image_name), torch.clamp(image, 0.0, 1.0),
                                     global_step=iteration)
                if not flag_gt_logged:
                    tb_writer.add_images("train_view_{}/ground_truth".format(train_viewpoint.image_name), torch.clamp(gt_image, 0.0, 1.0),
                                         global_step=iteration)
                tb_writer.add_images("environment_map",
                                     torch.sigmoid(scene.environment_map[:,:3,...]),
                                     global_step=iteration)
                if not flag_gt_logged:
                    tb_writer.add_images("train_view_{}/ground_truth".format(train_viewpoint.image_name),
                                         torch.clamp(gt_image, 0.0, 1.0), global_step=iteration)
                    if specular_environment_mask is not None:
                        tb_writer.add_images("train_view_{}/polytope_mask".format(train_viewpoint.image_name),
                                             polytope_mask, global_step=iteration)
                tb_writer.add_images("train_view_{}/diffuse_render".format(train_viewpoint.image_name),
                                     torch.clamp(diffuse_render[:,:3,:,:], 0.0, 1.0),
                                     global_step=iteration)
                if not args.diffuse_only:
                    tb_writer.add_images("train_view_{}/specular_render".format(train_viewpoint.image_name),
                                        torch.clamp(specular_render[:,:3,:,:], 0.0, 1.0),
                                        global_step=iteration)
                if scene.use_mask:
                    tb_writer.add_images("train_view_{}/mask".format(train_viewpoint.image_name),
                                         predicted_mask,
                                         global_step=iteration)
            flag_gt_logged = True
            tb_writer.add_scalar('train/loss_viewpoint - total_loss', total_loss, iteration)
            tb_writer.add_scalar('train/loss_viewpoint - l1_polytope_reg_loss', total_L1_polyreg_only, iteration)
            tb_writer.add_scalar('train/loss_viewpoint - l1_loss', total_L1, iteration)
            tb_writer.add_scalar('train/loss_viewpoint - LR_mask', total_LR_mask, iteration)
            tb_writer.add_scalar('train/loss_viewpoint - LR_specular_mask', total_LR_specular_mask, iteration)

            stop_iter_timer.record()
            torch.cuda.synchronize()
            print("[ITER {}] Train Loss {} [Elapsed: {}]".format(iteration, total_loss, start_iter_timer.elapsed_time(stop_iter_timer)))
            torch.cuda.empty_cache()


    iteration += 1
    sys.stdout.flush()
    
print("[ITER {}]Saving Model...".format(iteration))
# neural_renderer.save("./{}/neural_renderer/model_{}".format(tensorboard_folder, iteration))
scene.save(tensorboard_folder, iteration)

