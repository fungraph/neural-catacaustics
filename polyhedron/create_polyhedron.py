import os, sys
import numpy as np
import open3d as o3d
from PIL import Image

def getDir():
    return os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

sys.path.append(getDir())

from arguments.parse_args import get_args
from polyhedron.polyhedron_core import create_scene_polytope, render_mask
from scene_loaders.ibr_scene import Scene
from utils.general_utils import PILtoTorch
from utils.camera_utils import get_test_cameras


def main():
    args = get_args()

    debug_path = None

    resolution_scale = 1.0
    scene = Scene(args, resolution_scales=[resolution_scale], load_point_clouds=False)

    handmade_mask_path = os.path.join(args.input_path, "masks")

    reflection_estimation_masks = {}
    for cam in scene.getTrainCameras(resolution_scale):
        mask_path = os.path.join(handmade_mask_path, cam.image_name + ".png")
        mask = None
        if os.path.isfile(mask_path):
            mask_pil = Image.open(mask_path).convert('L')
            orig_w, orig_h = mask_pil.size
            scale = float(args.global_downscale) / orig_w
            resolution = (int(orig_w * scale), int(orig_h * scale))
            mask = PILtoTorch(mask_pil, resolution)
        reflection_estimation_masks[cam.image_name] = mask

    outPathPolytope = os.path.join(args.input_path, "polytope.obj")
    polytope, vizMeshes, meshes = create_scene_polytope(scene.getTrainCameras(resolution_scale), reflection_estimation_masks, debug_path)

    # output result
    print("Exporting polytope to ", outPathPolytope)
    o3d.io.write_triangle_mesh(outPathPolytope, polytope)

    o3d.visualization.draw_geometries(vizMeshes+meshes+[polytope], mesh_show_wireframe=True, mesh_show_back_face=True)

    #--------------------------------------------------
    # rendering masks

    print("Rendering masks...")
    train_outPathMasks = os.path.join(args.input_path, "train_polytope_masks", "")
    validation_outPathMasks = os.path.join(args.input_path, "validation_polytope_masks", "")
    test_outPathMasks = os.path.join(args.input_path, "test_polytope_masks", "")

    os.makedirs(train_outPathMasks, exist_ok=True)
    os.makedirs(validation_outPathMasks, exist_ok=True)
    os.makedirs(test_outPathMasks, exist_ok=True)

    for camIdx, camera in enumerate(scene.getTrainCameras(resolution_scale)):
        img = render_mask(camera, polytope)
        img = np.flip(np.asarray(img).T, 1)
        img = np.where(img == 1, 0, 1)
        img_pil = Image.fromarray((img * 255).astype(np.uint8))
        img_pil.save(os.path.join(train_outPathMasks, camera.image_name + ".png"))

        print("\rSaved mask %i/%i" % (camIdx+1, len(scene.getTrainCameras(resolution_scale))), end="")

    for camIdx, camera in enumerate(scene.getValidationCameras(resolution_scale)):
        img = render_mask(camera, polytope)
        img = np.flip(np.asarray(img).T, 1)
        img = np.where(img == 1, 0, 1)
        img_pil = Image.fromarray((img * 255).astype(np.uint8))
        img_pil.save(os.path.join(validation_outPathMasks, camera.image_name + ".png"))

        print("\rSaved mask %i/%i" % (camIdx+1, len(scene.getValidationCameras(resolution_scale))), end="")

    test_cameras = get_test_cameras(args)
    for camIdx, camera in enumerate(test_cameras):
        img = render_mask(camera, polytope)
        img = np.flip(np.asarray(img).T, 1)
        img = np.where(img == 1, 0, 1)
        img_pil = Image.fromarray((img * 255).astype(np.uint8))
        img_pil.save(os.path.join(test_outPathMasks, camera.image_name + ".png"))

        print("\rSaved mask %i/%i" % (camIdx+1, len(test_cameras)), end="")


    print("")


#==============================================

if __name__ == "__main__":
    main()
    print("=== TERMINATED ===")
