import os
import numpy as np
from datetime import datetime
import cv2
import random
import torch
import open3d as o3d
import pickle
import time
import tkinter as tk
from scipy.spatial import Delaunay
from tkinter import filedialog
from model_defs.warp_field_mlp import NormalizedMLP, CatacausticMLP, ProgressiveCatacausticMLP
from utils.ogl_ops import *
from arguments.parse_args import get_args
from scene_loaders.ibr_scene import Scene
from renderer.render_point_cloud import *
from utils.general_utils import polar2cart, cart2polar
from utils.graphics_utils import create_quad_plane_mesh, exportMeshAsObj, completeOrthogonalBasis
from scene_loaders.read_scenes_types import readBunderFolder
from utils.interactive_cameras import *


key_bindings = """
================================================
================================================
Neural Catacaustics Viewer

MODES
------
1     : Full Pytorch rendering
2     : Full OpenGL rendering (default)
3     : Diffuse-only Pytorch rendering
4     : Diffuse-only OpenGL rendering
5     : Specular-only Pytorch rendering
ESC   : Close application

CAMERA CONTROL
---------------
Left mouse      : Orbit
Right mouse     : Lateral motion
Middle mouse    : Dolly motion
c               : Toggle moving catacaustic camera only
x               : Reset catacaustic cam
v               : Snap to closest input view
s               : Toggle stereo rendering (only in render mode 1)
[]              : Decrease/increase stereo eye separation
()              : Decrease/increase stereo screen depth
u               : Toggle sanitized stereo
,               : Start/stop recording of camera path
.               : Play recorded camera path and save images

VISUALIZATION & OUTPUT
-----------------------
Ctrl + Left click   : Show visualization of catacaustic surface and UV space
z                   : Save screenshot and catacaustic surface mesh
r                   : Reload shaders
p                   : Toggle performance measurement
h                   : Show key bindings
m                   : Toggle rough specular material


================================================
================================================
"""



args = get_args()

#==============================================

# Convenience class that behaves like a dict but allows access with the attribute syntax
class EasyDict(dict):

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        del self[name]

#==============================================

renderData = EasyDict()

#==============================================

def saveImage(img, path, channels=3):
    if img.ndim == 2:
        outImg = img[..., None]
    if img.ndim == 3 and img.shape[2] >= 2:
        if channels == 2:
            outImg = np.zeros((*img.shape[0:2], 3))
            outImg[..., 1:3] = img[..., 2::-1]
        if channels == 3:
            outImg = img[..., 2::-1]
        if channels == 4:
            outImg = img[..., [2, 1, 0, 3]]
    if outImg.dtype == np.float32 or outImg.dtype == np.float64:
        outImg = np.clip(outImg, 0, 1) * 255
        outImg = outImg.astype(np.uint8)
    cv2.imwrite(path, outImg)

#==============================================

def savePickle(obj, path):
    pickle.dump(obj, open(path, "wb") )
    
def loadPickle(file):
    return pickle.load(open(file, "rb"))

#==============================================

def initShaders():
    renderData.displayOp = TexturedScreenQuadOP(renderData.res)
    renderData.pointRasterizationOp = PointRasterizationOP(renderData.res)
    renderData.visibilitySplatOp = VisibilitySplattingOP(renderData.res)
    renderData.compositingOp = CompositingOP(renderData.res)
    renderData.visualizationOverlayOp = VisualizationOverlayOP(renderData.res)
    renderData.pointCloud2DRenderOp = PointCloud2DRenderOP(renderData.visualizationRes)
    renderData.meshRenderOp = MeshRenderOP(renderData.visualizationRes)
    renderData.anaglyphOp = AnaglyphOP(renderData.res)

#==============================================

# initialize render environment
def initRenderEnvironment():

    renderData.fbo = createFramebuffer()
    initShaders()
    
    renderData.renderView = None
    renderData.outputView = None
    renderData.stereoViewL = None
    renderData.stereoViewR = None

    renderData.inputCamUVs = None
    renderData.inputCamUVsNormalized = None

    renderData.catacausticMesh = None

    renderData.leftMouseDown = False
    renderData.rightMouseDown = False
    renderData.centerMouseDown = False
    renderData.movePrimaryCam = True
    renderData.renderStereo = False
    renderData.stereoEyeSeparation = 0.8
    renderData.stereoScreenDepth = -0.01
    renderData.sanitizedStereo = False

    renderData.showPerformance = False
    renderData.moveCataCam = False
    renderData.selectedPoint = -1

    renderData.play_path = False
    renderData.play_path_frame = 0
    renderData.image_export_path = None
    renderData.record_path = False
    renderData.recorded_path = []
    renderData.played_path = []

    renderData.uncertainty_scale = 1.0

    if not os.path.exists(renderData.outputDir):
        os.makedirs(renderData.outputDir)

    root = tk.Tk()
    root.withdraw()

#==============================================

# save current view to disk
def saveCurrentView(filename, verbose=False, image_only=False):
    if renderData.outputView:
        img = downloadImage(renderData.outputView)
    else:
        img = downloadImage(renderData.renderView)
    filename = os.path.join(renderData.outputDir, filename)

    filename_image = filename + ".png"
    dirname = os.path.dirname(filename_image)

    if not os.path.exists(dirname):
        os.makedirs(dirname)
    
    saveImage(img, filename_image, channels=4)

    if image_only:
        return

    #savePickle(renderData.cam, filename + ".pkl")
    exportMesh = renderData.selectedPoint != -1
    if exportMesh:
        exportMeshAsObj(renderData.catacausticMesh, filename + ".obj")

    if verbose:
        txt = "Saved screenshot"
        if exportMesh:
            txt += " and mesh"
        print(txt + ":", filename)


#==============================================

def resetCataCam():
    renderData.cataCam = copy.deepcopy(renderData.cam)
    print("Reset catacaustic camera")

#==============================================

def snapToClosestInputCamera():
    
    c = renderData.cam.camera_center
    
    bestDst = 1e10
    bestIdx = -1

    for idx, tc in enumerate(renderData.train_cameras):
        dst = (tc.camera_center - c).norm(dim=1, p=2)[0]
        if dst < bestDst:
            bestDst = dst
            bestIdx = idx
    
    print("Snapping to train camera", bestIdx)
    bestCam = renderData.train_cameras[bestIdx]
    renderData.cam.updateCamera(bestCam.R, bestCam.T)
    
#==============================================

# key press action
# args: (key, x, y)  
def keyPressed(*args):

    def nowString():
        return datetime.utcnow().strftime("%Y%m%d%H%M%S")

    modeChange = False

    # esc
    if args[0] == b'\x1b':
        print("Exiting...")
        glutLeaveMainLoop()
    if args[0] == b'h':
        print(key_bindings)
    if args[0] == b'r':
        print("Reloading shaders...")
        initShaders()
    if args[0] == b'z':
        saveCurrentView("screenshot_" + nowString(), True)
    if args[0] == b'p':
        renderData.showPerformance = not renderData.showPerformance
    if args[0] == b'c':
        renderData.moveCataCam = not renderData.moveCataCam
        if renderData.moveCataCam:
            print("Moving only the catacaustic camera")
        else:
            print("Moving both cameras")
    if args[0] == b'x':
        resetCataCam()
    if args[0] == b'v':
        snapToClosestInputCamera()
    if args[0] == b'.':
        if not renderData.play_path:
            filetypes = [('Python pickle', '*.pkl')]
            play_file = filedialog.askopenfilename(filetypes=filetypes)
            if play_file:
                renderData.played_path = loadPickle(play_file)
                renderData.image_export_path = f"{nowString()}/"
                renderData.play_path = True
                print("Playing camera path and saving images...")
        else:
            renderData.play_path = False
            print("Stopped camera path.")
        renderData.play_path_frame = 0
    if args[0] == b',':
        renderData.record_path = not renderData.record_path
        if renderData.record_path:
            print("Starting recording...")
        else:
            print("\nStopped recording")
            filetype = [('Python pickle', '*.pkl')]
            recording_file = filedialog.asksaveasfilename(filetypes=filetype, defaultextension=filetype)
            if recording_file:
                savePickle(renderData.recorded_path, recording_file)
                print(f"Saved camera path to {recording_file}")
            renderData.recorded_path = []

    if args[0] == b's':
        renderData.renderStereo = not renderData.renderStereo
        print("Stereo " + ("on" if renderData.renderStereo else "off"))
    if args[0] == b'[':
        renderData.stereoEyeSeparation -= 0.2
        print(f"Eye separation: {renderData.stereoEyeSeparation:.1f}")
    if args[0] == b']':
        renderData.stereoEyeSeparation += 0.2
        print(f"Eye separation: {renderData.stereoEyeSeparation:.1f}")
    if args[0] == b'(':
        renderData.stereoScreenDepth -= 0.005
        print(f"Screen Depth: {renderData.stereoScreenDepth:.3f}")
    if args[0] == b')':
        renderData.stereoScreenDepth += 0.005
        print(f"Screen Depth: {renderData.stereoScreenDepth:.3f}")
    if args[0] == b'u':
        renderData.sanitizedStereo = not renderData.sanitizedStereo
        print("Sanitized Stereo " + ("on" if renderData.sanitizedStereo else "off"))
    if args[0] == b'm':
        renderData.uncertainty_scale = 20. if renderData.uncertainty_scale == 1. else 1.
    if args[0] == b'1' and not mode_is("FullTorch"):
        renderData.mode = "FullTorch"
        modeChange = True
    elif args[0] == b'2' and not mode_is("FullFast"):
        renderData.mode = "FullFast"
        modeChange = True
    elif args[0] == b'3' and not mode_is("DiffuseTorch"):
        renderData.mode = "DiffuseTorch"
        modeChange = True
    elif args[0] == b'4' and not mode_is("DiffuseFast"):
        renderData.mode = "DiffuseFast"
        modeChange = True
    elif args[0] == b'5' and not mode_is("Specular"):
        renderData.mode = "Specular"
        modeChange = True
    
    if modeChange:
        print("\nMode:", renderData.mode)

#==============================================

# mouse click action
def mouseClick(*args):
    button, down, x, y = args
    renderData.leftMouseDown = (button == 0 and down == 0)
    renderData.rightMouseDown = (button == 2 and down == 0)
    renderData.centerMouseDown = (button == 1 and down == 0)

    if renderData.leftMouseDown or renderData.rightMouseDown or renderData.centerMouseDown:
        renderData.lastClickPosition = np.array([x,y], dtype=np.float32)
        renderData.movePrimaryCam = renderData.selectedPoint == -1 or renderData.lastClickPosition[0] > renderData.visualizationRes

    if renderData.leftMouseDown and glutGetModifiers() == GLUT_ACTIVE_CTRL:
        selectPoint()
   
#==============================================

# mouse move action
def mouseMove(*args):
    
    currPos = np.array([*args], dtype=np.float32)
    offset = (currPos - renderData.lastClickPosition) / (0.5 * renderData.res[0])

    if renderData.movePrimaryCam:
        
        translSpeed = (0.5, 0.5, 1)
        rotSpeed = (3, 1, 3)

        t = np.eye(4)
        r = np.eye(4)

        if renderData.leftMouseDown:
            r = getRotationMatrix(rotSpeed * np.array([0, offset[1], -offset[0]]), renderData.rotationCenter)
        if renderData.rightMouseDown:
            t = getTranslationMatrix(translSpeed * np.array([offset[0], offset[1], 0]))
        if renderData.centerMouseDown:
            t = getTranslationMatrix(translSpeed * np.array([0, 0, offset[1]]))

        if not renderData.moveCataCam:
            applyCameraTransformations(renderData.cam, t=t, r=r)
        applyCameraTransformations(renderData.cataCam, t=t, r=r)

    else:

        rotSpeed = 7

        if renderData.leftMouseDown:
            r = getRotationMatrix(-rotSpeed * np.array([offset[0], offset[1], 0]), renderData.cataSurfaceCenter)
            renderData.meshCam = r @ renderData.meshCam

#==============================================

def runCatacausticEncoder(camCenters):
    cataMLP = renderData.scene.warp_field_mlp
    normalizedCams = cataMLP.normalize_cams(camCenters)
    raw_encoder_out = cataMLP.mlp_param(normalizedCams)
    if isinstance(cataMLP, CatacausticMLP):
        return cataMLP.uv_activate(raw_encoder_out)
    elif isinstance(cataMLP, ProgressiveCatacausticMLP):
        uvw = cataMLP.uvw_activate(raw_encoder_out)
        return cataMLP.sort_uvw(uvw)[:,:2]

#==============================================

def runCatacausticDecoder(uvs, points):
    cataMLP = renderData.scene.warp_field_mlp
    if isinstance(cataMLP, ProgressiveCatacausticMLP):
        uvs = torch.cat((uvs, torch.zeros_like(uvs[:,0:1])), dim=1)
    input_embed = torch.cat((uvs, points), dim=1)
    return cataMLP.mlp_embed(input_embed)
    
#==============================================

def normalizeUVs(uvs):
    return (uvs - renderData.uv_min) / (renderData.uv_max - renderData.uv_min)

def unnormalizeUVs(uvs):
    return uvs * (renderData.uv_max - renderData.uv_min) + renderData.uv_min

#==============================================

# select a catacaustic point based on a 2D click position
def selectPoint():

    def screenToWorldPosition(screenPos, cam):
        viewMatrix = cam.world_view_transformCPU[0]
        projMatrix = cam.projection_matrixCPU[0].numpy()    
        invCamMatrix = np.linalg.inv(projMatrix @ viewMatrix)
        screenPosNDC = 2 * screenPos / renderData.res - 1
        posWorld = invCamMatrix @ np.array([*screenPosNDC, 0, 1], dtype=np.float32)
        return posWorld[0:3] / posWorld[3] 

    with torch.no_grad():
        
        # warp all specular points
        specular_points = warp_point_cloud( renderData.scene.specular_point_cloud.global_xyz, 
                                            renderData.scene.warp_field_mlp, 
                                            renderData.cam,
                                            renderData.cataCam)
        point_count = specular_points.shape[0]
        
        # camera ray from click position
        camCenter = renderData.cam.camera_center.cpu().numpy()[0]
        reprojPos = screenToWorldPosition(renderData.lastClickPosition, renderData.cam)
        targetPos = camCenter + (reprojPos - camCenter) /  np.linalg.norm(reprojPos - camCenter)
        p1 = torch.from_numpy(camCenter).cuda().expand(point_count, 3)
        p2 = torch.from_numpy(targetPos).cuda().expand(point_count, 3)
        
        # select points close to the ray
        dRay = torch.cross(specular_points - p1, specular_points - p2, dim=1).norm(dim=1, p=2)
        thres = 0.025
        selInd = (dRay < thres).nonzero().flatten()
        if selInd.shape[0] == 0:
            renderData.selectedPoint = -1
            return
        selPos = specular_points[selInd]
        
        # from the selected points, select the one closest to the camera
        dCam = (selPos - p1[selPos.shape[0]]).norm(dim=1, p=2)
        closestInd = torch.argmin(dCam)
        renderData.selectedPoint = int(selInd[closestInd].cpu())

        #---------------------------------
        # visualize deformations 
        
        # vanilla MLP
        if isinstance(renderData.scene.warp_field_mlp, NormalizedMLP):
            
            region_opening_angle = np.radians(60)
            sampleCount = 100
            
            # determine the sampling volume
            p = renderData.polytopeCenter
            pcenter_to_cam = camCenter - p
            pcenter_to_cam /= np.linalg.norm(pcenter_to_cam)
            basis = completeOrthogonalBasis(pcenter_to_cam, up=np.array([0, 0, 1.]))
            sphericalCoords = []
            for tc in renderData.train_cameras:
                train_cam_center = tc.camera_centerCPU
                diff = train_cam_center - p
                angle = np.arccos(pcenter_to_cam.dot(diff / np.linalg.norm(diff)))
                if angle < region_opening_angle:
                    sphericalCoords.append(cart2polar(*(basis.dot(diff)))[0])
            sphericalCoords = np.array(sphericalCoords)
            minCoords = np.min(sphericalCoords, axis=0)
            maxCoords = np.max(sphericalCoords, axis=0)
            hull = Delaunay(sphericalCoords)
            is_inside_hull = lambda x: hull.find_simplex(x)>=0
            
            # rejection sampling of the volume
            samples = []
            failed_attempts = 0
            while len(samples) < sampleCount:
                candidate = np.random.rand(3) * (maxCoords - minCoords) + minCoords
                if is_inside_hull(candidate):
                    candidate_cart = np.transpose(basis).dot(polar2cart(*candidate)[0]) + p
                    samples.append(candidate_cart)
                else:
                    failed_attempts += 1
                    assert failed_attempts < 1e5, "Too many failed attempts during rejection sampling"
            samples = np.array(samples)
            #print(samples)

            
    
        
        # catacaustic MLP
        elif isinstance(renderData.scene.warp_field_mlp, CatacausticMLP) or isinstance(renderData.scene.warp_field_mlp, ProgressiveCatacausticMLP):
            # get catacaustic uv's for all input cameras
            if renderData.inputCamUVs is None:
                trainCamCount = len(renderData.train_cameras)
                # the first entry will be used for the render camera
                camCenters = torch.zeros(trainCamCount + 1, 3)
                for idx, tc in enumerate(renderData.train_cameras):
                    camCenters[idx + 1] = tc.camera_center[0]
                camCenters = camCenters.cuda()            
                renderData.inputCamUVs = runCatacausticEncoder(camCenters).cpu().numpy()
                renderData.uv_min = np.min(renderData.inputCamUVs, axis=0)
                renderData.uv_max = np.max(renderData.inputCamUVs, axis=0)
                renderData.inputCamUVsNormalized = normalizeUVs(renderData.inputCamUVs)

            # sample catacaustic surface
            uvSamples, tris = create_quad_plane_mesh(renderData.cataSurfaceRes)
            uvSamples = torch.from_numpy(unnormalizeUVs(uvSamples)).cuda()
            selPoint = renderData.scene.specular_point_cloud.global_xyz[renderData.selectedPoint].unsqueeze(0)
            vertices = runCatacausticDecoder(uvSamples, selPoint.expand(uvSamples.shape[0], 3)).cpu().numpy()
            
            # scale surface to unit range
            renderData.surfaceMean = np.mean(vertices, axis=0)
            vertices -= renderData.surfaceMean
            renderData.surfaceScale = np.max(np.abs(vertices))
            vertices /= renderData.surfaceScale

            # upload mesh to the GPU
            if renderData.catacausticMesh is not None:
                del renderData.catacausticMesh
                renderData.catacausticMesh = None
            renderData.catacausticMesh = OpenGLMesh(vertices, tris)
        
#==============================================

# the main render function
def showScreen():
     
    def torchToNumpy(t):
        n = t.cpu().numpy()[0]
        return np.transpose(n, (1, 2, 0))

    def numpyToTorch(n):
        n = np.transpose(n, (2, 0, 1)).copy()
        return torch.from_numpy(n).cuda().unsqueeze(0)
        
    if renderData.play_path:
        renderData.cam = renderData.played_path[renderData.play_path_frame][0]
        renderData.cataCam = renderData.played_path[renderData.play_path_frame][1]

    view_np = None

    glBindFramebuffer(GL_FRAMEBUFFER, renderData.fbo)
    startTime = time.time()
    
    with torch.no_grad():

        if renderData.movePrimaryCam:

            if mode_is("FullTorch"):
                if not renderData.renderStereo:
                    view = render_scene(
                        renderData.cam, renderData.scene, cata_camera=renderData.cataCam, 
                        uncertainty_scale=renderData.uncertainty_scale)[0]
                    view_np = torchToNumpy(view)
                else:
                    left_cam, right_cam = stereoFromMonoCam(
                        renderData.cam, renderData.res, 
                        eyeSeparation=renderData.stereoEyeSeparation, 
                        screenDepth=renderData.stereoScreenDepth)
                    viewL = render_scene(
                        left_cam, renderData.scene, 
                        cata_camera=renderData.cataCam if renderData.sanitizedStereo else left_cam, 
                        uncertainty_scale=renderData.uncertainty_scale)[0]
                    viewR = render_scene(
                        right_cam, renderData.scene, 
                        cata_camera=renderData.cataCam if renderData.sanitizedStereo else right_cam,
                        uncertainty_scale=renderData.uncertainty_scale)[0]
                    view_np = (torchToNumpy(viewL), torchToNumpy(viewR))
            
            if mode_is("DiffuseTorch"):
                _, view, _ = render_scene_diffuse_only(renderData.cam, renderData.scene, use_renderer=False)
                view_np = torchToNumpy(view)
                view_np[..., 3] = 1.

            if mode_is("Specular"):
                view = render_scene_specular_only(
                    renderData.cam, renderData.scene, cata_camera=renderData.cataCam,
                    uncertainty_scale=renderData.uncertainty_scale)
                view_np = torchToNumpy(view)
                view_np[..., 3] = 1.

            if mode_is(["DiffuseFast", "FullFast"]):
                depth1 = renderData.visibilitySplatOp.render(renderData.ogl_points, renderData.cam, 0)
                depth2 = renderData.visibilitySplatOp.render(renderData.ogl_points, renderData.cam, 1, depth1)
                splat_textures = renderData.pointRasterizationOp.render(renderData.ogl_points, renderData.cam, depth1, depth2)
                feats0_tex, feats1_tex = renderData.compositingOp.render(splat_textures)
            
            if mode_is("FullFast"):
                # OpenGL to Torch (there is probably a clever way to avoid this detour)
                feats0_np = downloadImage(feats0_tex)
                feats1_np = downloadImage(feats1_tex)
                feats_np = np.concatenate((feats0_np, feats1_np), axis=2)
                feats_torch = numpyToTorch(feats_np)

                # feed into the pipeline
                diffuse_render  = feats_torch[:, :7, ...]
                environment_mask = feats_torch[:, 7:, ...]
                view, _, _, _, _ = render_scene_from_OGL(
                    renderData.cam, 
                    renderData.scene, 
                    diffuse_render, 
                    environment_mask, 
                    cata_camera=renderData.cataCam,
                    uncertainty_scale=renderData.uncertainty_scale)
                view_np = torchToNumpy(view)

            if mode_is("DiffuseFast"):
                renderData.renderView = renderData.displayOp.render(feats0_tex, toScreen=False)
            else:
                if isinstance(view_np, tuple): # stereo
                    renderData.stereoViewL = uploadImage(view_np[0][..., 0:3], renderData.stereoViewL)
                    renderData.stereoViewR = uploadImage(view_np[1][..., 0:3], renderData.stereoViewR)
                    renderData.renderView = renderData.anaglyphOp.render(renderData.stereoViewL, renderData.stereoViewR)
                else:
                    renderData.renderView = uploadImage(view_np[..., 0:4], renderData.renderView)

    if renderData.selectedPoint == -1:
        renderData.outputView = None
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        renderData.displayOp.render(renderData.renderView, toScreen=True)

    else:
        with torch.no_grad():
            
            p = renderData.scene.specular_point_cloud.global_xyz[renderData.selectedPoint].unsqueeze(0)

            # ---- highlight selected point
            # warp selected point
            warped_viz_point = warp_point_cloud(p, 
                                                renderData.scene.warp_field_mlp, 
                                                renderData.cam,
                                                renderData.cataCam).cpu().numpy()[0]
            # project it
            viewMatrix = renderData.cam.world_view_transformCPU[0]
            projMatrix = renderData.cam.projection_matrixCPU[0].numpy()    
            proj_viz_point = projMatrix @ viewMatrix @ np.array([*warped_viz_point, 1])
            proj_viz_point = proj_viz_point[0:2] / proj_viz_point[3] * np.array([1, -1])
            pointPos = 0.5 * (proj_viz_point + 1) * renderData.res

            if isinstance(renderData.scene.warp_field_mlp, CatacausticMLP) or isinstance(renderData.scene.warp_field_mlp, ProgressiveCatacausticMLP):
                # ---- render view UVs as points
                camUV = runCatacausticEncoder(renderData.cam.camera_center)
                renderData.inputCamUVsNormalized[0] = normalizeUVs(camUV.cpu().numpy())
                renderData.point2DTex = renderData.pointCloud2DRenderOp.render(renderData.inputCamUVsNormalized)
                
                # ---- render catacaustic mesh
                cameraPoint = runCatacausticDecoder(camUV, p).cpu().numpy()[0]
                cameraPoint = (cameraPoint - renderData.surfaceMean) / renderData.surfaceScale
                renderData.meshTex = renderData.meshRenderOp.render(renderData.catacausticMesh, renderData.meshCam, cameraPoint)
            else:
                renderData.point2DTex = None
                renderData.meshTex = None

            # ---- compose visualization
            renderData.outputView = renderData.visualizationOverlayOp.render(
                renderData.renderView, 
                renderData.point2DTex, 
                renderData.meshTex, 
                pointPos)
            glBindFramebuffer(GL_FRAMEBUFFER, 0)
            renderData.displayOp.render(renderData.outputView, toScreen=True)
    
    if renderData.showPerformance:
        glFinish()
        endTime = time.time()
        time_ms = round((endTime - startTime) * 1000, 1)
        fps = 1000 / time_ms
        print("\rSpeed: %.1f ms | %.1f fps" % (time_ms, fps), end="")
    
    if renderData.play_path:
        renderData.play_path_frame += 1
        saveCurrentView(renderData.image_export_path + f"frame_{renderData.play_path_frame:06}", image_only=True)
        print(f"\rFrame {renderData.play_path_frame}/{len(renderData.played_path)}", end="")
        if renderData.play_path_frame == len(renderData.played_path):
            renderData.play_path = False
            renderData.play_path_frame = 0
            print(f"\nCamera path exported to {renderData.image_export_path}.")

    if renderData.record_path:
        renderData.recorded_path.append((copy.deepcopy(renderData.cam), copy.deepcopy(renderData.cataCam)))
        print(f"\rRecording frame {len(renderData.recorded_path)}", end="")

    glutSwapBuffers()

#==============================================

# convenience function to check the current render mode
def mode_is(modeList):
    if not isinstance(modeList, list):
        modeList = [modeList]
    return any(m in renderData.mode for m in modeList)

#==============================================

def loadScene():

    with torch.no_grad():
        device = torch.device("cuda:0")
        torch.cuda.set_device(device)
        torch.manual_seed(0)
        random.seed(0)

        renderData.scene = Scene(args, resolution_scales=[float(args.resolution_scale)], load_input_data=False)    
        
        polytope = o3d.io.read_triangle_mesh(os.path.join(args.input_path, "polytope.obj"))
        renderData.polytopeCenter = np.asarray(polytope.vertices).mean(axis=0)
        renderData.rotationCenter = renderData.polytopeCenter
    
#==============================================
from utils.camera_utils import cameraList_from_camInfos

def loadCameras(args, folder_name, scale=1.0, name_ints=8):
    camera_folder = os.path.join(args.input_path, folder_name)
    cam_infos = readBunderFolder(camera_folder, name_ints=name_ints)

    cams = cameraList_from_camInfos(cam_infos,
                                    scale, args,
                                    polytope_folder="test_polytope_masks",
                                    full_cam=False)
    return cams
#==============================================

def main():

    renderData.outputDir = "./screenshots/"
    ogl_data_path = os.path.join(args.scene_representation_folder, "ogl_data.npy")
    
    args.resolution_scale = 1.0
    args.global_downscale = 1000.0

    renderData.cataSurfaceRes = [10, 10]
    renderData.cataSurfaceCenter = [0, 0, -5]

    #--------------------------------

    print("Loading scene...")
    loadScene()

    print("Loading test cameras...")
    renderData.test_cameras = loadCameras(args, "test_path_cameras", scale=1.0, name_ints=5)
    renderData.train_cameras = loadCameras(args, "cropped_train_cameras", scale=1.0)

    renderData.cam = copy.deepcopy(renderData.test_cameras[0])
    resetCataCam()
    renderData.meshCam = getTranslationMatrix(renderData.cataSurfaceCenter)
    renderData.res = (renderData.cam.image_width, renderData.cam.image_height)
    renderData.visualizationRes = int(renderData.res[1] / 2)

    print("Setting up OpenGL...")
    oglInit(renderData.res, "Neural Catacaustics") 

    initRenderEnvironment()

    glutDisplayFunc(showScreen)
    glutIdleFunc(showScreen)  
    glutKeyboardFunc(keyPressed)
    glutMouseFunc(mouseClick)
    glutMotionFunc(mouseMove)

    print("Transferring diffuse point cloud to OpenGL...")
    renderData.ogl_points = OpenGLPointCloud(renderData.scene.diffuse_point_cloud, ogl_data_path)

    renderData.mode = "FullFast"
    print("Mode:", renderData.mode)

    print("Entering main loop...")
    print("Press h for help.")
    glutMainLoop() 


#==============================================

if __name__ == "__main__":
    main()
